const path = require('path');
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
// const Source = require('../models/source');

exports.getCompanies = (req, res, next) => {
    // const sources = req.body.source;
    // res.render('auth/signup', {
    //     pageTitle: 'Concept Group - Register',
    //     validationErrors: [],
    //     oldInput: [],
    //     sources: sources
    // });
    return "here";
};

exports.postCompany = async (req, res, next) => {
    const name = req.body.name;
    const description = req.body.description;
    const logo = req.body.logo;


    const errors = validationResult(req);

    // if (!errors.isEmpty()) {
    //     return res.status(422).render('auth/signup', {
    //         pageTitle: 'Concept Group - Register',
    //         hasError: true,
    //         //errorMessage: errors.array()[0].msg,
    //         validationErrors: errors.array(),
    //         oldInput: { firstname: firstname, middlename: middlename, email: email }
    //     });
    // }


    company = await new Company({
        name: name,
        description: description,
        logo: logo,
    });

    company = await company.save();

    return res.status(200).json({
        message: 'New Company was successfully added.',
        company: company
    });


};