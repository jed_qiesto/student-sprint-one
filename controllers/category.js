const path = require('path');
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
// const Source = require('../models/source');

exports.getCategories = (req, res, next) => {
    // const sources = req.body.source;
    // res.render('auth/signup', {
    //     pageTitle: 'Concept Group - Register',
    //     validationErrors: [],
    //     oldInput: [],
    //     sources: sources
    // });
    return "here";
};

exports.postCategory = async (req, res, next) => {
    const name = req.body.name;
    const description = req.body.description;


    const errors = validationResult(req);

    // if (!errors.isEmpty()) {
    //     return res.status(422).render('auth/signup', {
    //         pageTitle: 'Concept Group - Register',
    //         hasError: true,
    //         //errorMessage: errors.array()[0].msg,
    //         validationErrors: errors.array(),
    //         oldInput: { firstname: firstname, middlename: middlename, email: email }
    //     });
    // }


    category = await new Category({
        name: name,
        description: description,
    });

    category = await category.save();

    return res.status(200).json({
        message: 'New Category was successfully added.',
        category: category
    });


};