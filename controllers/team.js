const path = require('path');
const { validationResult } = require('express-validator');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Applicant = require('../models/applicant');
const Mission = require('../models/mission');
const Team = require('../models/team');
const Competency = require('../models/competency');
const Jobrole = require('../models/jobrole');
const SendMailController = require('./email');
// const {getAllNotifications} = require('../helpers/notificationHelper');

const ApplicantEmailEvent = require('../event/applicantEmailEvent');
const ApplicantNotificationEvent = require('../event/applicantNotificationEvent');

exports.getTeams = async (req, res, next) => {
    const mission = await Mission.find({ title: req.params.missionName });

    if (!mission){
        res.redirect('404');
    }


    res.render('mission/team', {
        pageTitle: 'Qiesto - Created Team',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
    });
};

exports.getCreateTeam = async (req, res, next) => {
    const mission = await Mission.findOne({ title: req.params.missionName })
                                    .populate('company')
                                    .populate('teams.team')
                                    .populate('categories.category')
                                    .populate('jobroles.jobrole');
    // console.log(mission);

    if (!mission) {
        res.redirect('404');
    }

    const myTeams = await Team.find({ 'team.members.member': res.locals.applicant._id }).setOptions({ lean: true });
    
    // const belongToATeamInProject = await Team.find({ 'team.members.member': res.locals.applicant._id, 'team.mission': mission._id });
    const belongToATeamInProject = await Team.find({ 'members.member': res.locals.applicant._id, 'mission': mission._id }).countDocuments();

    const jobroles = await Jobrole.find({});
    
    if (belongToATeamInProject !== 0){

        const missions = await Mission.find({ 'published': true }).populate('company').populate('categories.category');
        let redirectUrl = '/missions/interested/'+mission.title;
        return res.redirect(redirectUrl, 403, {
            pageTitle: 'Qiesto - Missions',
            errorMessage: 'You are already a member of a team in ' + mission.title ,
            successMessage: false,
            validationErrors: [],
            selected: mission._id,
            oldInput: [],
            missions: missions,
            notifications: [],
            unreadNotifications: []
        });
       
    }

    const competencies = await Competency.find();
    // console.log(`This is the competencies ${competencies[1]}`);

    res.render('mission/createTeam', {
        pageTitle: 'Qiesto - Created Team',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        mission: mission,
        myTeams: myTeams,
        competencies: competencies,
        jobroles,
        notifications: [],
        unreadNotifications: []
    });
};

exports.postCreateTeam = async (req, res, next) => {
    const teamName = req.body.teamName;
    const missionId = req.body.missionId;
    const who = req.body.who;
    // const myCompetencies = req.body.myCompetencies;
    const teamCompetencies = req.body.teamCompetencies;
    const textManifesto = req.body.textManifesto;
    const jobroles = req.body.jobroles;
    const errors = validationResult(req);
    let newTeamCompetencies = [];
    let newMyCompetencies = [];
    let newJobRoles = [];

    let applicantNotification = await Applicant.findOne({_id: res.locals.applicant._id});
    let notifications = applicantNotification.notifications;
    let unreadNotifications = applicantNotification.notifications.filter(notification=> notification.status === false)
    
    // console.log(textManifesto);

    // let  {notifications, unreadNotifications} = getAllNotifications(res.locals.applicant._id);
    

    let mission = await Mission.findOne({ _id: missionId})
                                .populate('company')
                                .populate('teams.team')
                                .populate('categories.category')
                                .populate('jobroles.jobrole');

    // console.log(mission);

    if (!mission){
        return res.status(422).render('mission/createTeam', {
            pageTitle: 'Qiesto - Create Team',
            hasError: true,
            errorMessage: 'Mission is not valid',
            successMessage: false,
            validationErrors: errors.array(),
            oldInput: { who: who, teamCompetencies: teamCompetencies,  teamName: teamName },
            notifications,
            notifications: mission,
            unreadNotifications,
        });
    }
    
    if (!errors.isEmpty()) {
        return res.status(422).render('mission/createTeam', {
            pageTitle: 'Qiesto - Create Team',
            hasError: true,
            errorMessage: errors.array()[0].msg,
            successMessage: false,
            validationErrors: errors.array(),
            oldInput: { who: who, teamCompetencies: teamCompetencies, teamName: teamName },
            notifications,
            notifications: mission,
            unreadNotifications,
        });
    }

    // For jobroles
    if (Array.isArray(jobroles)) {
        //MAP TEAM COMPETENCIES
        for (let jobrole of jobroles) //array is your array variable, i suppose
            newJobRoles.push({ jobrole: jobrole });
    } else {
        newJobRoles.push({ jobrole: jobrole });
    }

    // if (Array.isArray(schools)) {

    //CREATE NEW TEAM
    team = await new Team({
        mission: mission._id,
        name: teamName,
        missionStatus: false,
        textManifesto: textManifesto,
        who: who,
        type: 'Team',
        memberCount: 1,
        competencies: newTeamCompetencies,
        jobroles: newJobRoles
    });

    //save member details
    team.members = ({
        member: res.locals.applicant._id,
        membership: 'Member',
        position: 'Leader',
        competencies: newMyCompetencies
    })
    let membersEmail = team.members.member;
    team = await team.save();

    // console.log(`These are the team members ${membersEmail}`);
    //TODO:: Send email notification to everyone on team.members,
    //informing them of being added to the project; 

    // console.log(team);


    //UPDATE MISSION WITH TEAM ID
    let missionTeamExist = await Mission.find({ 'teams.team': team._id });

    if (missionTeamExist.length === 0) {
        //console.log('here');
        mission.teams.push({
            team: team._id
        });
        mission = await mission.save();
    }
   

    const myTeams = await Team.find({ 'members.member': res.locals.applicant._id }).populate('members.member').populate('mission');
    const competencies = await Competency.find();
    let redirectUrl = '/missions/interested/'+mission.title;

    // return res.redirect(200. redirectUrl);
    return res.redirect(redirectUrl, 200, {
        pageTitle: 'Qiesto - Created Team',
        errorMessage: false,
        successMessage: 'Your team has been succesfully created! You can invite interested students',
        validationErrors: [],
        oldInput: [],
        mission: mission,
        myTeams: myTeams,
        competencies: competencies,
        notifications,
        unreadNotifications
    });

};

exports.postCreateIndividualTeam = async (req, res, next) => {

    // let randomFourDigitNumber = Math.ceil(Math.random() * 1000);
    // const teamName = `${res.locals.applicant.firstname}${randomFourDigitNumber}`;
    const teamName = req.body.teamName;
    const missionId = req.body.missionId;
    const who = req.body.who;
    const textManifesto = req.body.textManifesto;

    // const myCompetencies = req.body.myCompetencies;
    // const teamCompetencies = req.body.myCompetencies;

    // const errors = validationResult(req);
    let newTeamCompetencies = [];
    let newMyCompetencies = [];
    // let newJobRoles = [];


    let mission = await Mission.findOne({ _id: missionId })
        .populate('company')
        .populate('teams.team')
        .populate('categories.category')
        .populate('jobroles.jobrole');


    if (!mission) {
        return res.status(422).render('mission/createTeam', {
            pageTitle: 'Qiesto - Create Team',
            hasError: true,
            errorMessage: 'Mission is not valid',
            successMessage: false,
            validationErrors: errors.array(),
            // oldInput: { who: who, teamCompetencies: teamCompetencies, myCompetencies: myCompetencies, teamName: teamName },

        });
    }


    // if (Array.isArray(teamCompetencies)) {
    //     //MAP TEAM COMPETENCIES
    //     for (let competency of teamCompetencies) //array is your array variable, i suppose
    //         newTeamCompetencies.push({ competency: competency, _id: mongoose.Types.ObjectId() });
    // } else {
    //     newTeamCompetencies.push({ competency: teamCompetencies, _id: mongoose.Types.ObjectId() });
    // }

    // if (Array.isArray(myCompetencies)) {
    //     //MAP TEAM COMPETENCIES
    //     //MAP MY COMPETENCIES
    //     for (let competency of myCompetencies) //array is your array variable, i suppose
    //         newMyCompetencies.push({ _id: mongoose.Types.ObjectId(), competency: competency });
    // } else {
    //     newMyCompetencies.push({ _id: mongoose.Types.ObjectId(), competency: myCompetencies });
    // }



    // if (Array.isArray(schools)) {

    //CREATE NEW TEAM
    let team = new Team({
        mission: mission._id,
        name: teamName,
        missionStatus: false,
        type: 'Individual',
        textManifesto: textManifesto,
        who: who,
        memberCount: 1,
        competencies: newTeamCompetencies,
    });


    //save member details
    team.members = ({
        member: res.locals.applicant._id,
        membership: 'Member',
        position: 'Leader',
        competencies: newMyCompetencies
    })

    let membersEmail = team.members.member;
    team = await team.save();

    // console.log('After saving the team');
   
    //UPDATE MISSION WITH TEAM ID
    let missionTeamExist = await Mission.find({ 'teams.team': team._id });

    if (missionTeamExist.length === 0) {
        //console.log('here');
        mission.teams.push({
            team: team._id
        });
        mission = await mission.save();
    }

   
    let redirectUrl = `/missions/mission/${missionId}`;

    // return res.redirect(200. redirectUrl);
    return res.redirect(redirectUrl, 200, {});
}

exports.getJoinTeam = async (req, res, next) => {
    let applicantNotification = await Applicant.findOne({_id: res.locals.applicant._id});
    let notifications = applicantNotification.notifications;
    let unreadNotifications = applicantNotification.notifications.filter(notification=> notification.status === false)
    
    const teamId = req.params.teamId;

    const team = await Team.findById(teamId)
                            .populate('members.member')
                            .populate({
                                path:'mission',
                                model: 'Mission',
                                populate: {
                                    path: 'company',
                                    model: 'Company'
                                }
                            });

    if (!team) {
        res.redirect('404');
    }

    let alreadyMemberofTeam = await Team.find({ '_id': teamId, 'members.member': res.locals.applicant._id}).countDocuments();

    let {mission} = team;
    if (alreadyMemberofTeam > 0){
    
        console.log('Already a member of this team');
    }

    const competencies = await Competency.find();
    // console.log(`This is the competencies ${competencies}`);

    res.render('mission/joinTeam', {
        pageTitle: 'Qiesto - Request to Join Team',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        team: team,
        competencies: competencies,
        mission
       
    });
};

exports.postJoinTeam = async (req, res, next) => {
    // const errors = validationResult(req);
    const teamId = req.body.teamId;
    const myCompetencies = req.body.myCompetencies;
    let newMyCompetencies = [];
    const competencies = await Competency.find();

    let team = await Team.findOne({ '_id': teamId }).populate('members.member').populate('mission');
    let members = team.members;
    let {mission} = team;

const leader = members.filter(m => m.position === 'Leader');
const leaderEmail = [];
const leaderName = [];
leader.forEach(r => leaderEmail.push(r.member.email));
leader.forEach(r => leaderName.push(r.member.firstname));
let memberDetail = await Applicant.findOne({_id:res.locals.applicant._id});


    // console.log(`The email is a type of ${leaderMember}`);

    // console.log(teamId);
    let existingRequest = await Team.findOne({ 'newMemberRequests.member': res.locals.applicant._id}).countDocuments();

    if (existingRequest > 0){
        return res.render('mission/joinTeam', {
            pageTitle: 'Qiesto - Request to Join Team',
            errorMessage: 'You have already sent a request to join '+ team.name,
            successMessage: false,
            validationErrors: [],
            oldInput: [],
            team: team,
            competencies: competencies,
            mission
        });

    }

    // const team = await Team.findById(teamId).populate('members.member').populate('mission');

    if (Array.isArray(myCompetencies)) {
        //MAP MY COMPETENCIES
        for (let competency of myCompetencies) //array is your array variable, i suppose
            newMyCompetencies.push({ _id: mongoose.Types.ObjectId(), competency: competency });
    } else {
        newMyCompetencies.push({ _id: mongoose.Types.ObjectId(), competency: myCompetencies });
    }


    //save member details
    team.newMemberRequests = ({
        member: res.locals.applicant._id,
        membership: 'Pending',
        position: 'Member',
        competencies: newMyCompetencies
    })
    team.notifications.push({
        _id: mongoose.Types.ObjectId(),
        brief: 'New member added',
        content: `A new member ${memberDetail.firstname} has requested team`,
        status: false,
        notificationType: `teams/${team.id}/${memberDetail.id}`
    });
    console.log('pushing to the team notifications');
    team = await team.save();
    // SendMailController.sendRequestToJoinTeam(leaderEmail, leaderName, res.locals.applicant.firstname);
    
    ApplicantEmailEvent.sendRequestToJoinTeam(leaderEmail, leaderName, res.locals.applicant.firstname);
    // team.newMemberRequests.push({
    //     member: res.locals.applicant._id,
    //     membership: 'Pending',
    //     position: 'Member',
    //     competencies: newMyCompetencies
    // });

    // //UPDATE CANDIDATE PROFILE
    // let applicant = await Applicant.findById(res.locals.applicant._id);

    // applicant.missions.push({
    //     mission: team.mission,
    //     membership: 'Pending',
    //     team: team._id,
    //     position: 'Member',
    //     competencies: newMyCompetencies,
    // });
    // applicant = await applicant.save();

    // team = await team.save();

    //TODO: Send email notification to the team lead for this person to join the team

    return res.render('mission/request-submitted', {
        pageTitle: 'Qiesto - Request to Join Team',
        errorMessage: false,
        successMessage: 'Your request to join '+ team.name +' has been succesfully sent',
        validationErrors: [],
        oldInput: [],
        team: team,
        competencies: competencies,
        mission
    });


};


exports.inviteMembers = async(req, res, next)=>{
    let teamId = req.params.teamId;


    let team = await Team.findOne({'_id': teamId}).populate('mission').populate('members.member');

    let mission = await Mission.findOne({ '_id': team.mission })
        .populate('interested.applicant')
        .populate('teams.team')
        .populate('company')
        .populate('categories.category')
        .populate('jobroles.jobrole');

    // let interestedApplicant = await Mission.find({'interested.applicant': res.locals.applicant._id, 'title': missionTitle }).countDocuments();


    // let {mission} = team;
    // console.log(mission);
    // let allApplicants = await Applicant.find();
    let allApplicants = team.members

    let {interested} = mission;
    let returnArray = [];
     allApplicants.filter(applicant=> {
      
      let returnVariable =  interested.filter(inter=>{
         
        // console.log(`This is inter ${inter.applicant} and this is applicant ${applicant._id}`);
        if(JSON.stringify(applicant.member._id) !== JSON.stringify(inter.applicant)){
            // console.log('They matched');
            return applicant.member;
        }
        return
      });
    //   console.log(`The return variable ${returnVariable}`);
      returnArray.push(returnVariable);
    })

    let repopulateApplicant = [];
    for(let i = 0; i<returnArray.length; i++){
        // console.log(`heehe ${returnArray[0][0].applicant}`);
        let newApplicant = await Applicant.findById(returnArray[0][i].applicant)
        repopulateApplicant.push(newApplicant);
    }
   
    let {sentInvites} = team;
    let allInterestedApplicants = mission.interested;
    let interestedApplicants = allInterestedApplicants.filter(interested => String(interested.applicant._id) !== String(res.locals.applicant._id));
    console.log('the sent invites are ')
    console.log(sentInvites);
    return res.render('mission/invite', {
                pageTitle: 'Qiesto - Select Team Members',
                applicants: repopulateApplicant,
                teamId: req.params.teamId,
                mission: mission,
                team: team,
                successMessage: false,
                errorMessage: false,
                interestedApplicants: interestedApplicants,
                sentInvites
            });
}


exports.addTeamMembers = async(req, res, next) =>{
    console.log(`This is the add checkbox ${req.body.addMemberCheckbox}`);
    console.log(`This is the teamId ${req.params.teamId}`);
    let teamId = req.params.teamId
    let team = await Team.findOne({_id: teamId}).populate('mission');
    let {mission} = team;
    // let currentTeam = allTeams.filter(team=> team._id == teamId);
    console.log(`This is the current team name ${team.name}`);
    let teamName = team.name
    let newMembersEmails = req.body.addMemberCheckbox;
    // console.log(req.body.addMemberCheckbox.length);
    let teamInviteArray = [];
    if (Array.isArray(newMembersEmails)){
        newMembersEmails.forEach(async(email)=>{
            let memberName = await Applicant.findOne({email: email});

           // Trigger an event to send email notifications
           ApplicantEmailEvent.sendTeamInvite(email, memberName.firstname, teamName);
           notification = {brief: `Team invite`, 
                                content: `You have been invited to join team ${teamName}, 
                                because you indicated interest in the mission ${team.mission.title}`, 
                                status: false,
                                notificationType: `missions/team/${team._id}`};
            memberName.notifications.push(notification);
            invite = {
                mission: team.mission._id,
                team: team._id,
            }
            memberName.invites.push(invite);
            memberName.save();
            applicantInterestArray = [{_id:mongoose.Types.ObjectId(), mission: team.mission._id , team: teamId }]
        let updateApplicant = await Applicant.updateOne({email: email}, 
            {invites: applicantInterestArray});
            console.log(`This is the updateApplicant ${updateApplicant}`);
            // Add to team invites
            teamInviteArray.push({applicant: memberName._id})
        });        
    } else {
        let memberName = await Applicant.findOne({email: newMembersEmails})
        ApplicantEmailEvent.sendTeamInvite(newMembersEmails, memberName.firstname, teamName);
        notification = {brief: `Team invite`, 
                             content: `You have been invited to join team ${teamName}, 
                             because you indicated interest in the mission ${team.mission.title}`, 
                             status: false, 
                            notificationType: `missions/team/${team._id}`};
         memberName.notifications.push(notification);
         memberName.save();
         applicantInterestArray = [{_id:mongoose.Types.ObjectId(), mission: team.mission._id , team: teamId }]
     let updateApplicant = await Applicant.updateOne({email: newMembersEmails}, 
         {invites: applicantInterestArray});
         console.log(`This is the updateApplicant ${updateApplicant}`);
         teamInviteArray.push({applicant: memberName._id});
    }
        // save team
        team.save();
    
    return res.render('mission/my-mission', {
        pageTitle: 'Qiesto - Missions',
        errorMessage: false,
        successMessage: 'Team invite sent successfully',
        applicants: [],
        teamId: req.params.teamId,
        mission,
        team
    });

    // return res.redirect
}

exports.addTeamMembersAndy = async(req, res, next) =>{
    let teamId = req.params.teamId
    let team = await Team.findOne({_id: teamId}).populate('mission');
    let {mission} = team;
    // let currentTeam = allTeams.filter(team=> team._id == teamId);
    console.log(`This is the current team name ${team.name}`);
    let teamName = team.name
    // let newMembersId = req.body.applicant;
    let newMembersId = req.params.applicantId;
    // console.log(req.body.addMemberCheckbox.length);
   
        let memberName = await Applicant.findOne({_id: newMembersId})
        SendMailController.sendTeamInvite(memberName.email, memberName.firstname, teamName);
        notification = {brief: `Team invite`, 
                             content: `You have been invited to join team ${teamName}, 
                             because you indicated interest in the mission ${team.mission.title}`, 
                             status: false, 
                            notificationType: `missions/team/${team._id}`};
         memberName.notifications.push(notification);
         memberName.save();
         applicantInterestArray = [{_id:mongoose.Types.ObjectId(), mission: team.mission._id , team: teamId }]
     let updateApplicant = await Applicant.updateOne({_id: newMembersId}, 
         {invites: applicantInterestArray});
        //  console.log(`This is the updateApplicant ${updateApplicant}`);
        //  teamInviteArray.push({applicant: memberName._id});

         ApplicantEmailEvent.sendJoinTeamNotification(memberName.email, memberName.firstname, team.name, memberName.firstname);
         let io = req.app.locals.io;
         console.log('Displaying the io details ', io);

         let applicant = memberName;
         let {notifications} = applicant;
        let unreadNotifications = notifications.filter(notification=> notification.status === false);

        
         io.emit(applicant._id, {unreadNotifications, notifications, applicant});
        await ApplicantNotificationEvent.triggerNotification(io, memberName);
        // save team
        team.sentInvites.push({applicant: memberName._id});
        team.save();
    
        return res.json({ success: true, message: 'Invite sent successfully' });

}

exports.getMyTeams = async (req, res, next) => {

    // const teams = await Applicant.find({ '_id': res.locals.applicant._id, 'mission.membership': 'Member' }).populate('members.member').populate('mission');
    const teams = await Team.find({ 'members.member': res.locals.applicant._id }).populate('members.member').populate('mission').populate('newMemberRequests.member');
    //console.log(teams);

    return res.render('mission/myTeams', {
        pageTitle: 'Qiesto - My Teams',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        teams: teams
    });
};

exports.approveMember = async (req, res, next) => {

    const teamId = req.params.teamId;
    const memberId = req.params.memberId;
    let memberDetail = await Applicant.findOne({_id: memberId});
    // console.log(memberId);

    const teams = await Team.find({ 'members.member': res.locals.applicant._id }).populate('members.member').populate('mission').populate('newMemberRequests.member');
    let team = await Team.findOne({ '_id': teamId });
    // console.log(team);

    // Check if member already exist
    let alreadyMemberofTeam = await Team.find({'members.member': memberId }).countDocuments();

    // get applicants competencies 
    let newMembercompetencies = team.newMemberRequests.filter(memberRequest => String(memberRequest.member) === String(memberId));
    if (alreadyMemberofTeam === 0) {
        //UPDATE NEW MEMBER LIST
        team.members.push({
            _id: mongoose.Types.ObjectId(),
            member: memberId,
            position: 'Member',
            membership :'Member',
            competencies: newMembercompetencies[0].competencies
        });
        
        team.memberCount = team.memberCount + 1;

        //UPDATE NEW MEMBER REQUEST
        // const oldMemberRequests = [... team.newMemberRequests ];
        const oldMemberRequests = team.newMemberRequests.filter(member => {
            // console.log(member.member.toString());
            return member.member.toString() !== memberId.toString();
        });
        // REMOVE MEMBER FROM MEMBER REQUEST LIST
        team.newMemberRequests = oldMemberRequests;
        // console.log(team);
        
        team.save();
    }

    if(team.members.length > 1){
    team.members.map(member=>{
    SendMailController.sendJoinTeamNotification(member.member.email, member.member.firstname, team.name, memberDetail.firstname);
    })
}
    return res.redirect('back');
    // return res.render('mission/myTeams', {
    //     pageTitle: 'Qiesto - My Teams',
    //     errorMessage: false,
    //     successMessage: 'Member request has been approved',
    //     validationErrors: [],
    //     oldInput: [],
    //     teams: teams
    // });
};

exports.getNotifications = async (req, res, next) =>{
    let teamId = req.params.teamId;
    let team = await Team.findById(teamId);

    let currentNotifications = team.notifications;

    const personalDetails = await Applicant.findById(res.locals.applicant._id);
    // const personalDetails = res.locals.applicant;
    const missions = await Mission.find().populate('categories.category').populate('company').limit(10);

    // console.log(personalDetails.work);

    let applicantNotification = await Applicant.findOne({_id: res.locals.applicant._id});
    let notifications = applicantNotification.notifications;
    let unreadNotifications = applicantNotification.notifications.filter(notification=> notification.status === false)
    res.render('mission/team-notifications', {
        pageTitle: 'Qiesto - Team Notification',
        errorMessage: false,
        successMessage: req.session.successMessage,
        validationErrors: [],
        oldInput: [],
        personalDetails: personalDetails,
        missions: missions,
        notifications,
        unreadNotifications,
        currentNotifications
    });
}

exports.getNewApplicant = async (req, res, next) =>{
    const teamId = req.params.teamId;
    const memberId = res.locals.applicant._id;
    let memberDetail = await Applicant.findOne({_id: memberId});
    // console.log(memberId);

    let competencies = await Competency.find({});
    // const teams = await Team.find({_id: req.params.teamId }).populate('members.member').populate('mission').populate('newMemberRequests.member');
    let team = await Team.findOne({ '_id': teamId }).populate('members.member').populate('mission').populate('company').populate('categories.category').populate('newMemberRequests.member');
    // console.log(team);

    // Check if member already exist
    let alreadyMemberofTeam = await Team.find({'members.member': memberId }).countDocuments();
    //console.log(alreadyMemberofTeam);

    let {mission} = team;
    console.log('here');
    console.log(mission);

    if (alreadyMemberofTeam === 0) {
        //UPDATE NEW MEMBER LIST
        // team.members.push({
        //     _id: mongoose.Types.ObjectId(),
        //     member: memberId,
        //     position: 'Member',
        //     membership :'Member'
        // });
        
        // team.memberCount = team.memberCount + 1;

        //UPDATE NEW MEMBER REQUEST
        // const oldMemberRequests = [... team.newMemberRequests ];
        // const oldMemberRequests = team.newMemberRequests.filter(member => {
            // console.log(member.member.toString());
            // return member.member.toString() !== memberId.toString();
        // });
        // REMOVE MEMBER FROM MEMBER REQUEST LIST
        // team.newMemberRequests = oldMemberRequests;
        // console.log(team);
        // let notification = {
        //     brief: 'New member added',
        //     content: `A new member ${memberDetail.name} has joined your team`
        // }
        // team.notifications.push(notification);
        // team.save();

        // team.members.map(member=>{
        //     ApplicantEmailEvent.sendJoinTeamNotification(member.member.email, member.member.firstname, team.name, memberDetail.firstname, teamId);
        // })

      return res.render('mission/fillCompetencies', {
            pageTitle: 'Qiesto - Competencies to Join Team',
            errorMessage: false,
            successMessage: false,
            validationErrors: [],
            oldInput: [],
            team: team,
            competencies: competencies,
            mission
           
        });

    }

   
    // return res.redirect('back');
    return res.render('mission/my-mission', {
        pageTitle: 'Qiesto - Mission',
        errorMessage: 'Already a member of the team',
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        team: team,
        notifications: team.notifications,
        mission
       
    });

}


exports.storeNewApplicant = async (req, res, next ) => {
    const teamId = req.body.teamId;
    const myCompetencies = req.body.myCompetencies;
    let newMyCompetencies = [];
    const competencies = await Competency.find();

    let team = await Team.findOne({ '_id': teamId }).populate('members.member').populate('mission');
    let members = team.members;
    let {mission} = team;

    let memberId = res.locals.applicant._id;
    memberDetail = res.locals.applicant;


    if (Array.isArray(myCompetencies)) {
        //MAP MY COMPETENCIES
        for (let competency of myCompetencies) //array is your array variable, i suppose
            newMyCompetencies.push({ _id: mongoose.Types.ObjectId(), competency: competency });
    } else {
        newMyCompetencies.push({ _id: mongoose.Types.ObjectId(), competency: myCompetencies });
    }

team.members.push({
            _id: mongoose.Types.ObjectId(),
            member: memberId,
            position: 'Member',
            membership :'Member',
            competencies: newMyCompetencies
        });
        
        team.memberCount = team.memberCount + 1;

        
        let notification = {
            brief: 'New member added',
            content: `A new member ${memberDetail.name} has joined your team`
        }
        team.notifications.push(notification);
        team.save();

        team.members.map(member=>{
            ApplicantEmailEvent.sendJoinTeamNotification(member.member.email, member.member.firstname, team.name, memberDetail.firstname, teamId);
        })

   
    console.log('pushing to the team notifications');
 
    res.render('mission/my-mission', {
        pageTitle: 'Qiesto - Mission',
        errorMessage: false,
        successMessage: 'You have been added to the team',
        validationErrors: [],
        oldInput: [],
        team: team,
        notifications: team.notifications,
        mission
       
    });  
}

exports.getNewApplicantRevised = async (req, res, next) => {
    const teamId = req.params.teamId;
    // const myCompetencies = req.body.myCompetencies;
    let newMyCompetencies = [];
    // const competencies = await Competency.find();

    let team = await Team.findOne({ '_id': teamId }).populate('members.member').populate('mission');
    console.log('this is the team ', team);
    // let members = team.members;
    let {mission} = team;

    let memberId = res.locals.applicant._id;
    memberDetail = res.locals.applicant;



team.members.push({
            _id: mongoose.Types.ObjectId(),
            member: memberId,
            position: 'Member',
            membership :'Member',
            // competencies: newMyCompetencies
        });
        
        team.memberCount = team.memberCount + 1;

        
        let notification = {
            brief: 'New member added',
            content: `A new member ${memberDetail.name} has joined your team`
        }
        team.notifications.push(notification);
        team.save();

        team.members.map(member=>{
            ApplicantEmailEvent.sendJoinTeamNotification(member.member.email, member.member.firstname, team.name, memberDetail.firstname, teamId);
        })

   
    console.log('pushing to the team notifications');
 
    res.render('mission/my-mission', {
        pageTitle: 'Qiesto - Mission',
        errorMessage: false,
        successMessage: 'You have been added to the team',
        validationErrors: [],
        oldInput: [],
        team: team,
        notifications: team.notifications,
        mission
       
    });  

}
exports.getRateMembers = async (req, res, next) => {
    

    let teamId = req.params.teamId;

    let team = await Team.findOne({ _id: teamId }).populate('mission').populate('members.member').populate('members.competencies.competency');
    let { mission } = team;
    let applicantId = res.locals.applicant._id;
    console.log('the applicant id ' + applicantId);
    let allApplicants = team.members;
    let allCompetencies = await Competency.find({});
    let filterApplicant = allApplicants.filter(member => JSON.stringify(member.member._id) !== JSON.stringify(applicantId))
  
    let selected = filterApplicant[0]._id;
    

    console.log('All the competencies ', allCompetencies);
    return res.render('mission/rate-members', {
        pageTitle: 'Qiesto - Rate Team Members',
        applicants: filterApplicant,
        teamId: req.params.teamId,
        successMessage: false,
        mission,
        errorMessage: false,
        interestedApplicants: filterApplicant,
        selected,
        allCompetencies
    });
}

exports.postRateMembers = async (req, res, next) => {
    let teamId = req.params.teamId
    let team = await Team.findOne({ _id: teamId }).populate('mission');

    // console.log('The body o')
    // console.log(req.body);
    let myCompetencies = req.body.competency;
    let newMyCompetencies = [];
    if (Array.isArray(myCompetencies)) {

        for (let competency of myCompetencies) //array is your array variable, i suppose
            newMyCompetencies.push({ _id: mongoose.Types.ObjectId(), competency: competency.competency });
    } else {
        newMyCompetencies.push({ _id: mongoose.Types.ObjectId(), competency: myCompetencies.myCompetencies.competency });
    }

    let insertData = {
        personRating: res.locals.applicant._id,
        personRated: req.body.applicantId,
        rating: newMyCompetencies
    }

    team.rateMembers.push(insertData);
    let results = await team.save();
    console.log('The result of saving it')
    console.log(results);
    return res.json({ success: true, message: 'Rating saved' });
  
}


exports.getCreateIndividualTeam = async (req, res, next) => {
    let missionId = req.params.missionId;

    let mission = await Mission.findOne({_id: missionId}).populate('teams.team');
    let  competencies = await Competency.find({});
    res.render('mission/acceptIndividualMission', {
        pageTitle: 'Qiesto - Mission',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        competencies,
        mission
       
    }); 
}

exports.postCreateIndividualTeam = async (req, res, next) => {
  
    let randomFourDigitNumber = Math.ceil(Math.random() * 1000);
    const teamName = `${res.locals.applicant.firstname}${randomFourDigitNumber}`;
    const missionId = req.body.missionId;
    
    const myCompetencies = req.body.myCompetencies;
    const teamCompetencies = req.body.myCompetencies;
    
    // const errors = validationResult(req);
    let newTeamCompetencies = [];
    let newMyCompetencies = [];
    let newJobRoles = [];

    
    // console.log(textManifesto);

    // let  {notifications, unreadNotifications} = getAllNotifications(res.locals.applicant._id);
    

    let mission = await Mission.findOne({ _id: missionId})
                                .populate('company')
                                .populate('teams.team')
                                .populate('categories.category')
                                .populate('jobroles.jobrole');

    // console.log(mission);
    
    if (!mission){
        return res.status(422).render('mission/createTeam', {
            pageTitle: 'Qiesto - Create Team',
            hasError: true,
            errorMessage: 'Mission is not valid',
            successMessage: false,
            validationErrors: errors.array(),
            // oldInput: { who: who, teamCompetencies: teamCompetencies, myCompetencies: myCompetencies, teamName: teamName },
            
        });
    }


    if (Array.isArray(teamCompetencies)) {
        //MAP TEAM COMPETENCIES
        for (let competency of teamCompetencies) //array is your array variable, i suppose
            newTeamCompetencies.push({ competency: competency, _id: mongoose.Types.ObjectId() });
    } else {
        newTeamCompetencies.push({ competency: teamCompetencies, _id: mongoose.Types.ObjectId() });
    }
   
    if (Array.isArray(myCompetencies)) {
        //MAP TEAM COMPETENCIES
        //MAP MY COMPETENCIES
        for (let competency of myCompetencies) //array is your array variable, i suppose
            newMyCompetencies.push({ _id: mongoose.Types.ObjectId(), competency: competency });
    } else {
        newMyCompetencies.push({ _id: mongoose.Types.ObjectId(), competency: myCompetencies });
    }
    
    

    // if (Array.isArray(schools)) {

    //CREATE NEW TEAM
    let team = new Team({
        mission: mission._id,
        name: teamName,
        missionStatus: false,
        type: 'individual',
        memberCount: 1,
        competencies: newTeamCompetencies,
    });

  
    //save member details
    team.members = ({
        member: res.locals.applicant._id,
        membership: 'Member',
        position: 'Leader',
        competencies: newMyCompetencies
    })
    let membersEmail = team.members.member;
    team = await team.save();

    console.log('After saving the team');
    // console.log(`These are the team members ${membersEmail}`);
    //TODO:: Send email notification to everyone on team.members,
    //informing them of being added to the project; 

    // console.log(team);


    //UPDATE MISSION WITH TEAM ID
    let missionTeamExist = await Mission.find({ 'teams.team': team._id });

    if (missionTeamExist.length === 0) {
        //console.log('here');
        mission.teams.push({
            team: team._id
        });
        mission = await mission.save();
    }

    // console.log('The individual team has been saved', team);

    // const myTeams = await Team.find({ 'members.member': res.locals.applicant._id }).populate('members.member').populate('mission');
    // const competencies = await Competency.find();
    let redirectUrl = `/missions/mission/${missionId}`;

    // return res.redirect(200. redirectUrl);
    return res.redirect(redirectUrl, 200, {


    });
}

exports.getViewMemberRating = async (req, res, next) => {
 
    let teamId = req.params.teamId;

    let team = await Team.findOne({ _id: teamId }).populate('rateMembers.rating.competency').populate('mission').populate('rateMembers.personRating');
  
    let { mission } = team;
    let applicantId = res.locals.applicant._id;
    // console.log('the applicant id ' + applicantId);
    let rateMembers = team.rateMembers;
  
    let allCompetencies = await Competency.find({});
    let filterApplicant = rateMembers.filter(member => JSON.stringify(member.personRated) === JSON.stringify(applicantId));
   
    let selected = false;
    if(filterApplicant.length > 0) {
        selected = filterApplicant[0]._id;
    }
    
    // console.log('View rate members ', filterApplicant[0].rating[0].competency);
    return res.render('mission/view-member-rating', {
        pageTitle: 'Qiesto - Rate Team Members',
        applicants: filterApplicant,
        teamId: req.params.teamId,
        successMessage: false,
        mission,
        errorMessage: false,
        interestedApplicants: filterApplicant,
        selected,
        allCompetencies
    });
}
