const path = require('path');
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const Applicant = require('../models/applicant');
const Team = require('../models/team');
const Source = require('../models/source');
const CustomToken = require('../helpers/authHelper');
let SendMailController = require('./email');
const moment = require('moment');
const Notification = require('../helpers/notificationHelper');
const ApplicantEmailEvent = require('../event/applicantEmailEvent');


exports.getNotify = async (req, res, next) => {

    res.render('auth/notify', {
        pageTitle: 'Qiesto - Register',
        messageTitle: 'Congratulations',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        callToAction: false,
        url: false,
        oldInput: [],
    });

};


exports.getSignup = async (req, res, next) => {
    if (res.locals.isAuthenticated) {
        return res.redirect('/missions');
    }
    const sources = await Source.find();

    res.render('auth/signup', {
        pageTitle: 'Qiesto - Register',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        sources: sources
    });
};

exports.postSignup = async (req, res, next) => {
    const sources = await Source.find();
    const firstname = req.body.firstname;
    const middlename = req.body.middlename;
    const lastname = req.body.lastname;
    const email = req.body.email;
    const source = req.body.source;
    let applicant;
   

    const errors = validationResult(req);
    // console.log(errors);

    if (!errors.isEmpty()) {
        console.log('here');
        // console.log(errors.array());
        return res.status(422).render('auth/signup', {
            pageTitle: 'Qiesto - Register',
            hasError: true,
            errorMessage: errors.array()[0].msg,
            successMessage: false,
            validationErrors: errors.array(),
            sources: sources,
            oldInput: { firstname: firstname, middlename: middlename, lastname: lastname, email: email }
        });
    }

    const hashedPassword = await bcrypt.hash(req.body.password, 12);

    applicant = new Applicant({
        firstname: firstname,
        middlename: middlename,
        lastname: lastname,
        email: email,
        about: '',
        password: hashedPassword,
        interests: [],
        profilePicture: 'assets/media/users/default.jpg',
        source: source
    });

    let applicantEmail = applicant.email;
    let applicantFirstName = applicant.firstname;
    const token = CustomToken.generateToken();
       applicant.verificationToken = token;
    applicant = await applicant.save();

    // Send a welcome email to the user
    ApplicantEmailEvent.sendVerificationMail(applicantEmail, applicantFirstName, token);
    
    // ApplicantEmailEvent.sendLoginMail(applicantEmail, applicantFirstName, token);

    return res.render('auth/notify', {
        pageTitle: 'Qiesto - Register',
        messageTitle: 'Congratulations',
        errorMessage: false,
        successMessage: 'Account has been successfully created. Kindly check your email for activation link!',
        validationErrors: [],
        callToAction: 'Proceed to Login',
        url: '/login',
        oldInput: [],
    });

};

exports.getLogin = async (req, res, next) => {
    if (res.locals.isAuthenticated) {
        return res.redirect('/missions');
    }
    res.render('auth/login', {
        pageTitle: 'Qieto - Login',
        errorMessage: false,
        successMessage: false,
        oldInput: {},
        validationErrors: []
    });
};

exports.postLogin = async (req, res, next) => {

    const email = req.body.email;
    const password = req.body.password;

    const errors = validationResult(req);
    // console.log(errors);

    if (!errors.isEmpty()) {
        //console.log(errors.array());
        return res.status(422).render('auth/login', {
            path: '/login',
            pageTitle: 'Login',
            errorMessage: errors.array()[0].msg,
            oldInput: {
                email: email,
                password: password
            },
            validationErrors: errors.array(),
        });
    }

    let applicant = await Applicant.findOne({ email: email });

    if (!applicant) {
        return res.status(422).render('auth/login', {
            path: '/login',
            pageTitle: 'Login',
            errorMessage: 'Invalid email or password.',
            oldInput: {
                email: email,
                password: password
            },
            validationErrors: [],
            successMessage: false,
        });
    }

    let authenticatedApplicant = await bcrypt.compare(password, applicant.password);

    if (!authenticatedApplicant) {
        
        return res.status(422).render('auth/login', {
            path: '/login',
            pageTitle: 'Login',
            errorMessage: 'Invalid email or password.',
            oldInput: {
                email: email,
                password: password
            },
            validationErrors: [],
            successMessage: false
        });
    }

    if (applicant.isVerified === false) {

        req.session.destroy(err => {
            return res.status(422).render('auth/login', {
                path: '/login',
                pageTitle: 'Login',
                errorMessage: 'Your account has not be verified. Kindly check your email for verification link!',
                oldInput: {
                    email: email,
                    password: password
                },
                validationErrors: [],
                successMessage: false
            });
        });
       
    }

    let teamNotifications = null;
    let applicantTeamId = null; 
    let team =  await Team.findOne({'members.member': applicant._id });
    if(team){
        teamNotifications = team.notifications;
        applicantTeamId = team._id;

    } 
    req.session.isLoggedIn = true;
    req.session.isVerified = applicant.isVerified;
    req.session.applicant = applicant;
    req.session.notifications = applicant.notifications;
    req.session.teamNotifications = teamNotifications;
    req.session.applicantTeamId = applicantTeamId;
    // console.log(applicant.about);
    // console.log(`session applicant team id ${req.session.applicantTeamId}`);
    // if (!applicant.about){
    //     return res.redirect('/settings');
    //     // return res.redirect('/setup/info');
    // }

    if (!applicant.interests){
        return res.redirect('/interest');
        // return res.redirect('/setup/interest');
    }

    return req.session.save(err => {
        res.redirect('/dashboard');
    });

};

exports.postLogout = (req, res, next) => {
    req.session.destroy(err => {
        console.log(err);
        res.redirect('/login');
    });
};

exports.getReset = (req, res, next) => {
    
        if (res.locals.isAuthenticated) {
            return res.redirect('/missions');
        }
        res.render('auth/getToken', {
            pageTitle: 'Qieto - Login',
            errorMessage: false,
            successMessage: false,
            oldInput: {},
            validationErrors: []
        });
  
}
exports.postReset = async (req, res, next) => {
    let applicantEmail = req.body.email;
    let applicant = await Applicant.findOne({email: applicantEmail});
    if(applicant){
       const token = CustomToken.generateToken();
       applicant.resetToken = token;
       applicant.resetTokenExpiration = moment().add(7, "days").format('YYYY-MM-DD');
       await applicant.save()
       ApplicantEmailEvent.sendResetPassword(applicantEmail, applicant.firstname, token);
       return res.render('auth/login', {
            path: '/login',
            pageTitle: 'Email-Sent',
            errorMessage: false,
            successMessage: 'Check your email for the token sent',
        })
    }
    return res.render('auth/getToken', {
        path: '/reset',
        pageTitle: 'Email-Sent',
        errorMessage: 'Email does not exist',
        successMessage: false,
    })
}

exports.getNewPassword = async(req, res, next)=> {
    let token = req.params.token;
    let applicant = await Applicant.findOne({resetToken: token});
    if(applicant){
     res.render('auth/newresetPassword', {
         path: '/login',
         pageTitle: 'Reset-Password',
         errorMessage: false,
         applicantName: applicant.firstname,
         applicantId: applicant._id,
         validationErrors: [],
         oldInput: []

     })   
    }
}

exports.postNewPassword = async(req, res, next) =>{
    let applicantId = req.body.applicantId;
    let password = req.body.password;
    let confirm_password = req.body.confirm_password;
    let applicantName = req.body.applicantName;
    
    const errors = validationResult(req);
    // console.log(errors);

    if (!errors.isEmpty()) {
        console.log('here');
        // console.log(errors.array());
        return res.status(422).render('auth/newresetPassword', {
            pageTitle: 'Qiesto - Update Password',
            hasError: true,
            errorMessage: errors.array()[0].msg,
            successMessage: false,
            validationErrors: errors.array(),
            oldInput: { password, confirm_password },
            applicantId: applicantId,
            applicantName: applicantName
        });
    }

    const hashedPassword = await bcrypt.hash(req.body.password, 12);
    let applicant = await Applicant.findOne({_id: applicantId});
    applicant.password = hashedPassword;
    applicant.resetToken = null;
    applicantTokenExpiration = null;
    await applicant.save();

    return res.status(201).render('auth/login', {
        pageTitle: 'Qiesto - Update Password',
        hasError: false,
        errorMessage: false,
        successMessage: 'Your password has been updated',
        validationErrors: null,
        oldInput:null,
        applicantId: applicantId,
        applicantName: applicantName
    });
}


////////////////////////////////////
//////// Email verification ///////
//////////////////////////////////

exports.getEmailVerification = async(req, res, next)=> {
    let token = req.params.token;
    let applicant = await Applicant.findOne({verificationToken: token, isVerified: false});
    if(!applicant){
        // return res.redirect('/404');  
        return res.render('auth/notify', {
            pageTitle: 'Qiesto - Register',
            messageTitle: 'Hello there!',
            errorMessage: 'There seems to be some mixup but this account has already been verified!',
            successMessage: false,
            validationErrors: [],
            callToAction: 'Proceed to Login',
            url: '/login',
            oldInput: [],
        });
    }

    // const applicantEmail = applicant.email;
    // const applicantEmail = applicant.firstname;
    // const token = CustomToken.generateToken();

    //Mark as verified
    applicant.isVerified = true;
    await applicant.save();

    ApplicantEmailEvent.sendLoginMail(applicant.email, applicant.firstname, CustomToken.generateToken());


    return res.render('auth/notify', {
        pageTitle: 'Qiesto - Register',
        messageTitle: 'Hello there!',
        errorMessage: false,
        successMessage: 'Your email has been successfully verified!',
        validationErrors: [],
        callToAction: 'Proceed to Login',
        url: '/login',
        oldInput: [],
    });
};

////////////////////////////////////
//////// Terms and Conditions ///////
//////////////////////////////////
exports.getTermsAndConditions = async(req, res, next)=> {
   
    return res.render('no-auth/terms-and-conditions', {
         path: '/login',
         pageTitle: 'login',
         errorMessage: false,
        
         errorMessage: false,
         successMessage: false,
         validationErrors: null,
         oldInput:null,

     }) 
}