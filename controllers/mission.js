const path = require('path');
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const Applicant = require('../models/applicant');
const Mission = require('../models/mission');
const Company = require('../models/company');
const Category = require('../models/category');
const Team = require('../models/team');
const Jobrole = require('../models/jobrole');
const SendEmailController = require('../controllers/email');
const Competencies = require('../models/competency');
const ApplicantEmailEvent = require('../event/applicantEmailEvent');
// const deepPopulate = require('mongoose-deep-populate')(mongoose);
// const deepPopulate = require('mongoose-deep-populate');

const moment = require('moment');


exports.getMission = async (req, res, next) => {


    let page = req.query.page;
     // page ? page : 0;
     if(page === undefined || Number(page) === 0) {
        page =  1;
      }

    const missionId = req.params.missionId;


    // console.log(missionId);

    const allMissions = await Mission.find({ 'published': true }).populate('company').populate('categories.category').populate('jobroles.jobrole').limit(6);

    const mission = await Mission.findOne({ '_id': missionId })
        .populate('company')
        .populate('categories.category')
        .populate('jobroles.jobrole');

    // const teamMember = await Team.findOne({ 'members.member': applicant._id, 'team.mission': mission._id });
    const team = await Team.findOne({ 'mission': mission._id, 'members.member': res.locals.applicant._id })
        .populate({
            path: 'members.member',
            select: 'firstname lastname _id missions'
        })
        .populate({
            path: 'newMemberRequests.competencies.competency',
            model: 'Competency'
            // select: 'firstname lastname _id missions'
        })
        .populate({
            path: 'newMemberRequests.member',
            select: 'firstname lastname _id'
        });

    // console.log(teamMember);

    let {truncData, paginationLength} = truncateResponse(allMissions, page, 4 );
    console.log(paginationLength);
    let missions = truncData;

    if (!team){
        res.render('mission/mission', {
            pageTitle: 'Qiesto - Mission',
            errorMessage: false,
            successMessage: false,
            validationErrors: [],
            oldInput: [],
            missions: missions,
            mission: mission,
            notifications: [],
            unreadNotifications: [],
            pageSelected: page,
            paginationLength,
        });
    } else {
    // console.log('this is the rate members ', team.rateMembers);

        res.render('mission/my-mission', {
            pageTitle: 'Qiesto - Mission',
            errorMessage: false,
            successMessage: false,
            validationErrors: [],
            oldInput: [],
            mission: mission,
            team: team,
            notifications: team.notifications,
            unreadNotifications: [],
            newMemberRequests: team.newMemberRequests,
            pageSelected: page,
            paginationLength,
        });
    }
    
};

exports.getMissions = async (req, res, next) => {
    //console.log('here');
    let selected = req.query.selected;
    let page = req.query.page;

    if (!selected){
        selected = false;
    }

    // page ? page : 0;
    if(page === undefined || Number(page) === 0) {
      page =  1;
    }
    console.log('the page is ', page);

    const numberOfMissions = 8;

    const allMissions = await Mission.find({ 'published': true }).populate('company').populate('categories.category').populate('jobroles.jobrole');
    //console.log(missions);
    let {truncData, paginationLength} = truncateResponse(allMissions, page, numberOfMissions );
    console.log(paginationLength);
    let missions = truncData;
    res.render('mission/list', {
        pageTitle: 'Qiesto - Missions',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        missions: missions,
        selected: selected,
        notifications: [],
        unreadNotifications: [],
        pageSelected: page,
        paginationLength,

    });
};


function truncateResponse (data, startIndex, amount) {
    let startingPoint = --startIndex * amount;
    let truncData = data.slice(startingPoint, (startingPoint + amount));    
    
    let paginationLength = Math.ceil(data.length / amount);
    return {truncData, paginationLength};
}

exports.getMyMissions = async (req, res, next) => {
   
    let selected = req.query.selected;
    // console.log(selected);
    let missions;
    // missions = await Mission.find({ 'published': true }).populate('company').populate('categories.category').populate('jobroles.jobrole');
    missions = await Team.find({ 'members.member': res.locals.applicant._id })
    .populate({
        path: 'mission',
        model: 'Mission',
        populate: {
            path: 'company',
            model: 'Company'
        }
    }).populate({
        path: 'mission',
        model: 'Mission',
        populate: {
            path: 'categories.category',
            model: 'Category'
        }
    });
    // console.log(missions);

    //  console.log('the mission notifications ', missions);


    
    res.render('mission/my-missions', {
        pageTitle: 'Qiesto - Missions',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        missions: missions,
        team : missions,
        //notifications: missions.notifications
    });
};

exports.getInterested = async (req, res, next) => {
    const missionTitle = req.params.missionName;
    //console.log(missionTitle);

    let mission = await Mission.findOne({ title: missionTitle })
        .populate('interested.applicant')
        .populate('teams.team')
        .populate('company')
        .populate('categories.category')
        .populate('jobroles.jobrole');

        // console.log(mission);

   
    if (!mission){
        res.redirect('/404');
    }
    //console.log(mission);
   
    let interestedApplicant = await Mission.find({'interested.applicant': res.locals.applicant._id, 'title': missionTitle }).countDocuments();
    // console.log(interestedApplicant);
    
    if (interestedApplicant === 0){
        // console.log('we are here');
        mission.interested.push({
            applicant: res.locals.applicant._id
        });
        mission = await mission.save();
    }

    // console.log('Those interested in this mission are ');
    // console.log(mission.interested);

    let applicantNotification = await Applicant.findOne({_id: res.locals.applicant._id});

    let unfilteredApplicants = mission.interested;
    let filteredApplicants = unfilteredApplicants.filter(appl => String(appl.applicant._id) !== String(res.locals.applicant._id) )
    return res.render('mission/interested', {
        pageTitle: 'Qiesto - Mission - ' + mission.title,
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        missionName: mission.title,
        teams: mission.teams,
        mission: mission,
        interestedApplicants: filteredApplicants
       
    });


};


exports.getMyMission = async (req, res, next) => {

    // return res.redirect('back');

    const missionId = req.params.missionId;
   
    const mission = await Mission.findOne({ '_id': missionId })
                                 .populate('company')
                                 .populate('categories.category')
                                 .populate('jobroles.jobrole');


    const team = await Team.findOne({ 'mission': missionId, 'members.member': res.locals.applicant._id })
                            .populate({
                               path: 'members.member',
                               select: 'firstname lastname _id missions'
                            })
                            .populate({
                                path: 'newMemberRequests.competencies.competency',
                                model: 'Competency'
                                // select: 'firstname lastname _id missions'
                            })
                            .populate({
                                path: 'newMemberRequests.member',
                                select: 'firstname lastname _id'
                            });


    return res.render('mission/my-mission', {
        pageTitle: 'Qiesto - Missions',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        mission: mission,
        team: team,
        notifications: team.notifications
        // newMemberRequests: newMemberRequests
    });
};

exports.getCompleteMission = async (req, res, next) =>{
    let teamId = req.params.teamId;
    let missionId = req.params.missionId;
    let competencies = await Competencies.find();
    let mission = await Mission.findOne({_id:missionId})
        .populate('company')
        .populate('categories.category')
        .populate('jobroles.jobrole');
    let team = await Team.findOne({_id: teamId}).populate('mission');
    // let {mission} = team;
    // console.log(`This is the team ${team}`);
   return res.render('mission/complete-mission', {
    pageTitle: 'Qiesto - Missions',
    errorMessage: false,
    successMessage: false,
    validationErrors: [],
    oldInput: [],
    team,
    mission,
    competencies: competencies,

   })
}

exports.postCompleteMission = async (req, res) => {
    let teamId = req.params.teamId;
    let missionReport = req.body.report;
    const errors = validationResult(req);
    let competencies = await Competencies.find();
    
    let team = await Team.findOne({_id: teamId}).populate('mission').populate('members.member');
    if (!team){
        req.redirect('404');
     }
    let members = team.members
    let teamMembersEmail =[]
    let io = req.app.locals.io;
    team.members.map(member=>
        ApplicantEmailEvent.sendMissionComplete(member.member.email, member.member.firstname, team.mission.title, team.name, teamId)
        );
    
    let mission = team.mission;
    let notification = {
        brief: 'Mission Completed',
        content: `The mission ${mission.title} has been completed by your team. Congratulations in advance`
    }
    team.notifications.push(notification);
    

    if (!errors.isEmpty()) {
        return res.status(422).render('mission/complete-mission', {
            pageTitle: 'Qiesto - Complete Mission',
            hasError: true,
            errorMessage: errors.array()[0].msg,
            successMessage: false,
            validationErrors: errors.array(),
            oldInput: { report: report },
            mission,
            team,
            competencies
        });
    }

    console.log(`This is the report ${req.body.report}`);

    team.missionReport = missionReport;
    team.missionStatus = true;
    team.save();
    
    // return res.render('mission/complete-mission', {
    //     pageTitle: 'Qiesto - Missions',
    //     errorMessage: false,
    //     successMessage: 'You have successfully completed this mission',
    //     validationErrors: [],
    //     oldInput: [],
    //     team,
    //     mission
    
    //    })

    // return res.redirect(`/missions/completed/${teamId}`);

    return res.render('mission/my-mission', {
        pageTitle: 'Qiesto - Complete Mission',
        successMessage: 'Report submitted successfully',
        mission,
        team,
        members,
        errorMessage: false
    })
}

exports.getCompletedMission = async(req, res, next) => {
    let teamId = req.params.teamId;

    let team = await Team.findOne({_id: teamId}).populate('mission').populate('members.member');
    let mission = team.mission;
    let members = team.members;
    return res.render('mission/viewCompleted', {
        pageTitle: 'Qiesto - Complete Mission',
        mission,
        team,
        members,
        successMessage: false
    })
}


exports.getTeams = async (req, res, next) => {
    let misionId = req.params.missionId;

    let mission = await Mission.findOne({_id: misionId}).populate('teams.team');
    let {teams} = mission;
    console.log('these are the teams ', teams);

    return res.render('mission/viewTeams', {
        pageTitle : 'Qiesto - Teams',
        mission,
        teams
    })
}

exports.getByFilter = async (req, res, next) => {
    let type = req.params.type;
    let name = req.params.name;

    // console.log('the name and type are ', type);
    let allMissions = await Mission.find({'published': true}).populate('company').populate('categories.category').populate('jobroles.jobrole');
    // const allMissions = await Mission.find({ 'published': true }).populate('company').populate('categories.category'); 
    
    console.log('showing all truncated Missions');
    // console.log( truncated);
    const convertTypeToUpper = String(type).toUpperCase();
    let returnedSearchResults = [];
    let resultsBasedOn = `${name} from `;
    switch(convertTypeToUpper) {
        case 'CATEGORY': 
        allMissions.map(allMission => {
                let dummyArry = allMission.categories.filter(allM => String(allM.category.name) === name );
                // console.log('the dummmy Arry');
                // console.log(dummyArry);
                if(dummyArry.length > 0) {
                    returnedSearchResults.push(allMission);
                } 
                 } );
                 resultsBasedOn += `Categories`;
        break;

        case 'MISSION': 
        allMissions.map(allMission => {
            if(String(allMission.title) === String(name)){
                console.log(allMission.title)
             returnedSearchResults.push(allMission);
            }});
            resultsBasedOn += ` Missions`;
        break;

        case 'COMPANY': 
        console.log('i a in the company');
         allMissions.map(allMission => {
            
            // console.log(allMission.company);
                if(allMission.company.name === name) {
                    console.log('compare these two ')
                    // console.log(allMission.company.name);
                    console.log(allMission)
                    returnedSearchResults.push(allMission);
                    // return allMission
                } 
                 } );
                 resultsBasedOn += ` Companies`;
        break;
    default: 
    break
    }

    // let truncated = truncateResponse(returnedSearchResults, 1, 5);
    // console.log('the returned search results ');
    // console.log(returnedSearchResults);

    let page = 1;
    let {truncData, paginationLength} = truncateResponse(returnedSearchResults, page, 100 );
    let selected = truncData[0];
    console.log(paginationLength);
    let missions = truncData;
    res.render('mission/searchPage', {
        pageTitle: 'Qiesto - Missions',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        missions: missions,
        selected: selected,
        // notifications: [],
        // unreadNotifications: [],
        pageSelected: page,
        paginationLength,
        resultsBasedOn

    });
}


exports.getAdvanceSearch = async (req, res, next) => {
    //console.log('here');
 
    res.render('mission/advanceSearch', {
        pageTitle: 'Qiesto - Missions',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: []

    });
};


exports.postAdvanceSearch = async (req, res, next) => {
   
    const missionName = req.body.missionName;
    const category = req.body.category;
    const startDate = req.body.startDate;
    const endDate = req.body.endDate;

    console.log(missionName);
    let allMissions = await Mission.find({'published': true}).populate('company').populate('categories.category').populate('jobroles.jobrole');

    console.log(req.body.startDate);
   

    let filteredMission = filterByMissionName(allMissions, missionName);
    filteredMission = filterByCategory(filteredMission, category);
    filteredMission = filterByStartDate(filteredMission, startDate);
    filteredMission = filterByEndDate(filteredMission, endDate);

    // console.log('displaying results for filtered ')
    // console.log(filteredMission);

    let resultsBasedOn = 'advance search';
    let page = 1;
    let {truncData, paginationLength} = truncateResponse(filteredMission, page , 100 );
    
    let selected = truncData[0];
    console.log(paginationLength);
    let missions = truncData;
    res.render('mission/searchPage', {
        pageTitle: 'Qiesto - Missions',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        missions: missions,
        selected: selected,
        // notifications: [],
        // unreadNotifications: [],
        pageSelected: page,
        paginationLength,
        resultsBasedOn

    });
}

const filterByMissionName = (missions, missionName) => {
    let filteredMissions = missions.filter(mission => String(mission.title) === String(missionName));
    
    if(filteredMissions.length > 0) {
        return filteredMissions;
    } 
    return missions;
}

const filterByCategory = (missions, category) => {
    let returnedSearchResults = [];
    missions.map(mission => {
        let dummyArry = mission.categories.filter(allM => String(allM.category.name) === String(category) );
       
        if(dummyArry.length > 0) {
            // console.log('the dummmy Arry');
            // console.log(dummyArry);
            returnedSearchResults.push(mission);
        } 
         } );
       
    if(returnedSearchResults.length > 0) {
        console.log('the category was found')
        return returnedSearchResults;
    } 

    return missions;
}

const filterByStartDate = (missions, date) => {
    console.log('the date is ', date);
    let filteredMissions = []
    if(String(date).length > 0){
    let  rearrangeDate = flipDate(date);
    filteredMissions = missions.filter(mission => moment(mission.startDate).isSameOrAfter(rearrangeDate));
 
        console.log('the date is later than it');
        return filteredMissions;
    }
    return missions;
}

const filterByEndDate = (missions, date) => {
    let filteredMissions = []
    if(String(date).length > 0){
    let rearrangeDate = flipDate(date);
    filteredMissions = missions.filter(mission => moment(mission.endDate).isSameOrBefore(rearrangeDate));
  
        return filteredMissions;
    }
    return missions;
}

const flipDate = (date) => {
    const year = String(date).substring(6);
    const month = String(date).substring(3, 5);
    const day = String(date).substring(0, 2);

    return `${year}-${month}-${day}`;
}


exports.editCompleteMission = async (req, res, next) =>{
    let teamId = req.params.teamId;
    let missionId = req.params.missionId;
    let competencies = await Competencies.find();
    let mission = await Mission.findOne({_id:missionId})
        .populate('company')
        .populate('categories.category')
        .populate('jobroles.jobrole');
    let team = await Team.findOne({_id: teamId}).populate('mission');
    // let {mission} = team;
    // console.log(`This is the team ${team}`);
   return res.render('mission/edit-mission-report', {
    pageTitle: 'Qiesto - Missions',
    errorMessage: false,
    successMessage: false,
    validationErrors: [],
    oldInput: [],
    team,
    mission,
    competencies: competencies,

   })
}


exports.updateCompleteMission = async (req, res) => {
    let teamId = req.params.teamId;
    let missionReport = req.body.report;
    const errors = validationResult(req);
    let competencies = await Competencies.find();
    
    let team = await Team.findOne({_id: teamId}).populate('mission').populate('members.member');
    if (!team){
        req.redirect('404');
     }
     if (!errors.isEmpty()) {
        return res.status(422).render('mission/complete-mission', {
            pageTitle: 'Qiesto - Complete Mission',
            hasError: true,
            errorMessage: errors.array()[0].msg,
            successMessage: false,
            validationErrors: errors.array(),
            oldInput: { report: report },
            mission,
            team,
            competencies
        });
    }

    let members = team.members
    let teamMembersEmail =[]
    let io = req.app.locals.io;
    team.members.map(member=>
        ApplicantEmailEvent.sendMissionComplete(member.member.email, member.member.firstname, team.mission.title, team.name, teamId)
        );
   
    let mission = team.mission;
    let notification = {
        brief: 'Mission Completed',
        content: `The mission report (${mission.title} )has been modified by your team. `
    }
    team.notifications.push(notification);
    

   

    console.log(`This is the report ${req.body.report}`);

    team.missionReport = missionReport;
    team.missionStatus = true;
    team.save();
    
    // return res.render('mission/complete-mission', {
    //     pageTitle: 'Qiesto - Missions',
    //     errorMessage: false,
    //     successMessage: 'You have successfully completed this mission',
    //     validationErrors: [],
    //     oldInput: [],
    //     team,
    //     mission
    
    //    })

    // return res.redirect(`/missions/completed/${teamId}`);

    return res.render('mission/my-mission', {
        pageTitle: 'Qiesto - Complete Mission',
        successMessage: 'Report updated successfully',
        mission,
        team,
        members,
        errorMessage: false
    })
}
