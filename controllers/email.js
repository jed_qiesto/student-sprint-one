'use strict';
// const nodeMailer = require('nodemailer');
const nodeMailer = require('nodemailer');
const nodemailMailgun = require("nodemailer-mailgun-transport");
const Email = require('email-templates');
const path = require('path');

// Setting up authentication
    const auth = {
        auth:{
        api_key:'1f649ca2306e5f6114afff1141c63c6e-73ae490d-19504fd4', 
            // domain:'mailgun.rosabon-finance.com' 
            domain:'email.qiesto.com' 
        }
    }

    let transporter = nodeMailer.createTransport(nodemailMailgun(auth));
    const email = new Email({
      transport: transporter,
      send: true,
      preview: false,
      views: {
        options: {
          extension: 'ejs',
        },
        // root: './loginemails',
      },
    });

exports.sendLoginMail = function( recipientEmail, recipientFirstName, token){    
    email.send({
      template: path.join(__dirname, 'loginEmails', 'emailtemplates') ,
      message: {
        from: 'accounts@qiesto.com',
        to: recipientEmail,
      },
      locals: {
        fname: recipientFirstName,
        token: token
      }
    }).then(() => console.log('email has been sent!'));   
};

exports.sendVerificationMail = function( recipientEmail, recipientFirstName, token){    
    email.send({
      template: path.join(__dirname, 'verificationEmails', 'emailtemplates') ,
      message: {
        from: 'accounts@qiesto.com',
        to: recipientEmail,
      },
      locals: {
        fname: recipientFirstName,
        token: token
      }
    }).then(() => console.log('email has been sent!'));   
};

exports.sendRequestToJoinTeam = function(recipientEmail, recipientName, applicantName){
  email.send({
    template: path.join(__dirname, 'joinTeamEmails', 'emailtemplates') ,
    message: {
      from: 'accounts@qiesto.com',
      to: recipientEmail,
    },
    locals: {
      applicantName: applicantName,
      recipientName: recipientName
      
    }
  }).then(() => console.log('email has been sent!'));   
};

exports.sendTeamInvite = function(recipientEmail, recipientName, teamName){
  email.send({
    template: path.join(__dirname, 'teamInviteEmails', 'emailtemplates') ,
    message: {
      from: 'accounts@qiesto.com',
      to: recipientEmail,
    },
    locals: {
      recipientName: recipientName,
      teamName: teamName
    }
  }).then(() => console.log('email has been sent!')); 
};

exports.sendResetPassword = (recipientEmail, recipientName, token) =>{
  email.send({
    template: path.join(__dirname, 'resetPasswordEmail', 'emailtemplates') ,
    message: {
      from: 'accounts@qiesto.com',
      to: recipientEmail,
    },
    locals: {
      recipientName: recipientName,
      token:token
    }
  }).then(() => console.log('email has been sent!'));
};

exports.sendMissionComplete = (recipientEmail, recipientName, missionTitle, teamName, teamId) =>{
  email.send({
    template: path.join(__dirname, 'missionCompletedEmail', 'emailtemplates') ,
    message: {
      from: 'accounts@qiesto.com',
      to: recipientEmail,
    },
    locals: {
      recipientName,
      recipientEmail,
      missionTitle,
      teamName,
      teamId
    }
  }).then(() => console.log('email has been sent!'));
};

exports.sendJoinTeamNotification = (recipientEmail, recipientName, teamName, memberId, teamId) => {
  email.send({
    template: path.join(__dirname, 'teamEmailNotification', 'emailtemplates') ,
    message: {
      from: 'accounts@qiesto.com',
      to: recipientEmail,
    },
    locals: {
      recipientName,
      recipientEmail,
      teamName,
      teamId,
      memberId
    }
  }).then(() => console.log('email has been sent!'));
};