const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const mongoose = require('mongoose');
const csrf = require('csurf');
const flash = require('connect-flash');

const errorController = require('./controllers/error');

const DB_USER = 'root';
const PASSWORD = encodeURIComponent('Qiesto@11#');
// const DB_URL = `mongodb://${DB_USER}:${PASSWORD}@ds331735.mlab.com:31772/any_db`;

// const MONGODB_URI = 'mongodb://testAdmin:test#@localhost:27017/qiestomvc?authSource=admin';
const MONGODB_URI = `mongodb://${DB_USER}:${PASSWORD}@localhost:27017/qiestomvc?authSource=admin`;

const app = express();
const store = new MongoDBStore({
    uri: MONGODB_URI,
    collection: 'sessions'
});
const csrfProtection = csrf();


app.set('view engine', 'ejs');
app.set('views', 'views');

const authRoutes = require('./routes/auth');
const missionRoutes = require('./routes/mission');
const applicantRoutes = require('./routes/applicant');

app.use(bodyParser.urlencoded({ extended: false }));


// app.use('/static', express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, 'public')));
// app.use(express.static(__dirname + '/public'));
app.use('/public/img', express.static(path.join(__dirname, 'img')));
// app.use('/public/css', express.static(path.join(__dirname, 'css')));
// app.use('/public/css', express.static(path.join(__dirname, 'css')))

app.use(
    session({
        secret: 'my secret',
        resave: false,
        saveUninitialized: false,
        store: store
    })
);

app.use(csrfProtection);
app.use(flash());

app.use((req, res, next) => {
    res.locals.isAuthenticated = req.session.isLoggedIn;
    res.locals.applicant = req.session.applicant;
    app.locals.moment = require('moment');
    res.locals.csrfToken = req.csrfToken();
    next();
});
//app.use(bodyParser.json()); //application json

app.use('/', authRoutes);
app.use('/', missionRoutes);
app.use('/', applicantRoutes);
app.use(errorController.get404);


mongoose
    .connect(MONGODB_URI, { useNewUrlParser: true })
    .then(result => {
        // console.log('Connected to Database!')
        app.listen(8080);
    })
    .catch(err => {
        console.log(err);
    });

