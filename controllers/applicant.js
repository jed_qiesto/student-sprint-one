const path = require('path');
const { validationResult } = require('express-validator');
// const bcrypt = require('bcryptjs');
const Applicant = require('../models/applicant');
const mongoose = require('mongoose');
const Mission = require('../models/mission');
const Company = require('../models/company');
const Category = require('../models/category');
const Team = require('../models/team');
const Country = require('../models/country');
const Interest = require('../models/interest');
const moment = require('moment');
const Competency = require('../models/competency');


exports.getSettings = async (req, res, next) => {

    const interests = await Interest.find().sort({ name: 1 });
    const countries = await Country.find();

    const personalDetails = res.locals.applicant;

    // console.log('this is the applicant details', personalDetails);

    

    // console.log(personalDetails.interests.find(e => e.interest === '5d4523b1205edc0afd200b4') ? 'true' : ' ');

    res.render('applicant/settings', {
        pageTitle: 'Qiesto - Settings',
        errorMessage: false,
        successMessage: req.session.successMessage,
        validationErrors: [],
        oldInput: [],
        personalDetails: personalDetails,
        countries: countries,
        interests: interests
    });
};


exports.postSettings = async (req, res, next) => {

    const personalDetails = res.locals.applicant;

    const interests = await Interest.find();
    const countries = await Country.find();
    const errors = validationResult(req);

    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const middlename = req.body.middlename;
    const about = req.body.about;
    const gender = req.body.gender;
    const facebook = req.body.facebook;
    const twitter = req.body.twitter;
    const linkedin = req.body.linkedin;
    const birthday = req.body.birthday;
    const phoneNumber = req.body.phoneNumber;
    const country = req.body.country;
    const occupation = req.body.occupation;
    // const setupType = req.body.setupType;
    const image = req.file;

    // console.log(image);



    const newInterests = req.body.interests;
    // console.log(newInterests);
    // const other = req.body.other;
    // const setupType = req.body.setupType;
    let selectedInterests = [];



    if (!errors.isEmpty()) {
        // if (setupType === "settings"){
            return res.render('applicant/settings', {
                pageTitle: 'Qiesto - Settings',
                errorMessage: errors.array()[0].msg,
                successMessage: false,
                validationErrors: errors.array(),
                oldInput: [],
                personalDetails: personalDetails
            });
        // } else {
        //     return res.render('setup/info', {
        //         pageTitle: 'Qiesto - Settings',
        //         errorMessage: errors.array()[0].msg,
        //         successMessage: false,
        //         validationErrors: errors.array(),
        //         oldInput: [],
        //         personalDetails: personalDetails
        //     });
        // }
        
    }

    if (Array.isArray(newInterests)) {
        // MAP INTERESTS
        for (let interest of newInterests) //array is your array variable, i suppose
        selectedInterests.push({ interest: interest, _id: mongoose.Types.ObjectId() });
    } else {
        // interests.push({ interest: newInterests, _id: mongoose.Types.ObjectId() });
        selectedInterests.push({ interest: newInterests, _id: mongoose.Types.ObjectId() });
    }

    

    let accountDetails = await Applicant.findOne({'_id': personalDetails._id});
        accountDetails.firstname = firstname;
        accountDetails.lastname = lastname;
        accountDetails.middlename = middlename;
        accountDetails.gender = gender;
        accountDetails.birthday = moment(birthday, 'DD-MM-YYYY').format('YYYY-MM-DD');
        accountDetails.about = about;
        accountDetails.phoneNumber = phoneNumber;
        accountDetails.country = country;
        accountDetails.facebook = facebook;
        accountDetails.twitter = twitter;
        accountDetails.linkedin = linkedin;
        accountDetails.occupation = occupation;
        accountDetails.interests = selectedInterests;
        if (image){
            accountDetails.profilePicture = image.path.replace('public/', '');
        }
    accountDetails.save();


    // if (setupType === "settings") {
    //     return req.session.reload(function (err) {
    //         req.session.applicant = accountDetails;
    //         req.session.successMessage = 'Changes to your account details has been succesfully made!';
    //         res.redirect('/settings', 200, {
    //             pageTitle: 'Qiesto - Settings',
    //             errorMessage: false,
    //             successMessage: 'Changes to your account details has been succesfully made!',
    //             validationErrors: [],
    //             oldInput: [],
    //             personalDetails: accountDetails
    //         });
    //     });
    // } else {

        req.session.applicant = accountDetails;

        // return req.session.reload(function (err) {
        //     res.redirect('/dashboard');
        // });

        return res.render('applicant/settings', {
            pageTitle: 'Qiesto - Settings',
            errorMessage: false,
            successMessage: 'Your profile has been successfully updated',
            validationErrors: errors.array(),
            oldInput: [],
            personalDetails: req.session.applicant,
            countries: countries,
            interests: interests
        });
    // }
    

};

exports.getEducation = async (req, res, next) => {

    // const missions = await Mission.find({ 'published': true }).populate('company').populate('categories.category');
    //console.log(missions);

    // const personalDetails = res.locals.applicant;

    const interests = await Interest.find();
    const countries = await Country.find();

    const personalDetails = res.locals.applicant;


    // console.log(personalDetails);

    return res.render('applicant/education', {
        pageTitle: 'Qiesto - Settings',
        errorMessage: false,
        successMessage: req.session.successMessage,
        validationErrors: [],
        oldInput: [],
        personalDetails: personalDetails,
        countries: countries,
        interests: interests,
    });
};



exports.postEducation = async (req, res, next) => {

    //const school = req.body.school;
    let schools = req.body.school;
    let courses = req.body.course;
    let startYears = req.body.startYear;
    let endYears = req.body.endYear;
    let education = [];


    if (Array.isArray(schools)){
        for (c = 0; c < schools.length; c++) {
            // console.log('is array');
            education.push({ school: schools[c], course: courses[c], startYear: startYears[c], endYear: endYears[c], _id: mongoose.Types.ObjectId() });
        }
    } else {
            // console.log('is not array');
        education.push({ school: schools, course: courses, startYear: startYears, endYear: endYears, _id: mongoose.Types.ObjectId() });        
    }

    // console.log(education);

    // console.log('here again');
    // console.log(schools);
    let applicant = await Applicant.findOne({ '_id': res.locals.applicant._id});
    // let applicant = await Applicant.findById(res.locals.applicant._id);
        applicant.education = education;
    applicant.save();

    // req.session.successMessage = false;

    // return req.session.reload(function (err) {
    //     // req.session.applicant = accountDetails;
    //     // req.session.successMessage = '';
    //     res.redirect('/settings/education', 200, {
    //         pageTitle: 'Qiesto - Settings',
    //         errorMessage: false,
    //         successMessage: 'Education has been successfully updated!',
    //         validationErrors: [],
    //         oldInput: [],
    //     });
    // });

    req.session.applicant = applicant;
    // const personalDetails = applicant;

    // console.log(req.session.applicant);


    return res.render('applicant/education', {
        pageTitle: 'Qiesto - Settings',
        errorMessage: false,
        successMessage: 'Education has been successfully updated!',
        personalDetails: req.session.applicant,
        validationErrors: [],
        oldInput: [],
    });
};

exports.getWork = async (req, res, next) => {

    // const missions = await Mission.find({ 'published': true }).populate('company').populate('categories.category');
    //console.log(missions);

    const personalDetails = res.locals.applicant;

    // console.log(personalDetails);

    return res.render('applicant/work', {
        pageTitle: 'Qiesto - Settings',
        errorMessage: false,
        successMessage: req.session.successMessage,
        validationErrors: [],
        oldInput: [],
        personalDetails: personalDetails
    });
};

exports.postWork = async (req, res, next) => {

    //const school = req.body.school;
    let employers = req.body.employer;
    let roles = req.body.role;
    let startYears = req.body.startYear;
    let endYears = req.body.endYear;
    let descriptions = req.body.description;
    let work = [];


    if (Array.isArray(employers)) {
        for (c = 0; c < employers.length; c++) {
            work.push({ employer: employers[c], role: roles[c], startYear: startYears[c], endYear: endYears[c], description: descriptions[c],  _id: mongoose.Types.ObjectId() });
        }
    } else {
        work.push({ employer: employers, role: roles, startYear: startYears, endYear: endYears, description: description, _id: mongoose.Types.ObjectId() });
    }

    let applicant = await Applicant.findById(res.locals.applicant._id);
        applicant.work = work;
    applicant.save();

    // res.locals.applicant = applicant;

    req.session.applicant = applicant;

    return res.render('applicant/work', {
        pageTitle: 'Qiesto - Settings',
        errorMessage: false,
        successMessage: 'Work Experience has been successfully updated',
        validationErrors: [],
        oldInput: [],
        personalDetails: req.session.applicant
    });

};


exports.getSetupInfo = async (req, res, next) => {
    const personalDetails = res.locals.applicant;

    res.render('setup/info', {
        pageTitle: 'Qiesto - Settings',
        errorMessage: false,
        successMessage: req.session.successMessage,
        validationErrors: [],
        oldInput: [],
        personalDetails: personalDetails,
        notifications: [],
        unreadNotifications: []
        
    });
}

exports.getSetupInterest = async (req, res, next) => {
    // const personalDetails = res.locals.applicant;
    const interests = await Interest.find();

    res.render('setup/interest', {
        pageTitle: 'Qiesto - Settings',
        errorMessage: false,
        successMessage: req.session.successMessage,
        validationErrors: [],
        oldInput: [],
        interests: interests,
        notifications: [],
        unreadNotifications: []
    });
}

exports.postInterest = async (req, res, next) => {

    const personalDetails = res.locals.applicant;

    const errors = validationResult(req);
    const newInterests = req.body.interests;
    const other = req.body.other;
    const setupType = req.body.setupType;
    let interests = [];

    // console.log(newInterests);


    if (!errors.isEmpty()) {
        if (setupType === "settings") {
            return res.render('applicant/interets', {
                pageTitle: 'Qiesto - Settings',
                errorMessage: errors.array()[0].msg,
                successMessage: false,
                validationErrors: errors.array(),
                oldInput: [],
                personalDetails: personalDetails
            });
        } else {
            return res.render('setup/interest', {
                pageTitle: 'Qiesto - Settings',
                errorMessage: errors.array()[0].msg,
                successMessage: false,
                validationErrors: errors.array(),
                oldInput: [],
                personalDetails: personalDetails
            });
        }

    }

    if (Array.isArray(newInterests)) {    
        // MAP INTERESTS
        for (let interest of newInterests) //array is your array variable, i suppose
            interests.push({ interest: interest, _id: mongoose.Types.ObjectId() });
    } else {
        interests.push({ interest: newInterests, _id: mongoose.Types.ObjectId() });
    }

    // console.log(interests);

    let accountDetails = await Applicant.findOne({ '_id': personalDetails._id });
        accountDetails.interests = interests;
        accountDetails.interests.other = other;
    accountDetails.save();

    if (setupType === "settings") {
        return req.session.reload(function (err) {
            req.session.applicant = accountDetails;
            req.session.successMessage = 'Changes to your account details has been succesfully made!';
            res.redirect('/settings', 200, {
                pageTitle: 'Qiesto - Settings',
                errorMessage: false,
                successMessage: 'Changes to your account details has been succesfully made!',
                validationErrors: [],
                oldInput: [],
                personalDetails: accountDetails
            });
        });
    } else {
        return req.session.reload(function (err) {
            res.redirect('/dashboard');
        });
    }


};

exports.dashboard = async (req, res, next) => {
    
    const personalDetails = await Applicant.findById(res.locals.applicant._id);
    // const personalDetails = res.locals.applicant;
    const missions = await Mission.find().populate('categories.category').populate('company').limit(10);

    // console.log(personalDetails.work);

    let applicantNotification = await Applicant.findOne({_id: res.locals.applicant._id});
    let notifications = applicantNotification.notifications;
    let unreadNotifications = applicantNotification.notifications.filter(notification=> notification.status === false)
    res.render('applicant/dashboard', {
        pageTitle: 'Qiesto - Dashboard',
        errorMessage: false,
        successMessage: req.session.successMessage,
        validationErrors: [],
        oldInput: [],
        personalDetails: personalDetails,
        missions: missions,
        notifications,
        unreadNotifications
    });
}

exports.getInvites = async(req, res, next)=>{
    let userId = res.locals.applicant._id;
    let {invites} = await Applicant.findOne({_id: userId});
    let unacceptedInvites = invites.filter(invite=> invite.accepted === undefined);
    let helpme = null;
    let teams = [];
    for(let i= 0; i<unacceptedInvites.length; i++ ){
        let getTeams = await Team.findOne({_id: unacceptedInvites[i].team}).populate('mission');
        // console.log(`This is the get teams ${getTeams}`);
        teams.push(getTeams);
        // console.log(`The teams in the for loop ${teams}`);
    }
    // console.log(`The teams outside the loop ${teams}`);
    let selected = false;
    if(teams[0]){
        let selected = teams[0]._id;
    }
    return res.render('applicant/teamInvite', {
        pageTitle: 'Qiesto - Team Invites',
        teams: teams,
        errorMessage: false,
        selected: selected

    })

}

exports.acceptInvite = async(req, res, next) => {
   let applicantId = res.locals.applicant._id;
   let teamId = req.params.teamId;
   let newMember = {_id: mongoose.Types.ObjectId(),membership:'member', position: 'member', member: applicantId};
//    let updateTeam = await Team.updateOne({_id: teamId}, {members: newMember}, {upsert: true});
    let updateTeam = await (await Team.findOne({_id: teamId})).populated('mission');
//    let returnTeam =  updateTeam.members.push(newMember);
    // await updateTeam.save()
//    console.log(`This is the update team ${updateTeam}`);
//    let updateApplicant = await Applicant.updateOne({_id: applicantId}, {$unset:{invites}})
    let applicant = await Applicant.findOne({_id: applicantId});
    let applicantMissions = {_id:mongoose.Types.ObjectId(), membership: 'member', mission:updateTeam.mission, team: updateTeam._id, position: 'member'};
    // let applicantInvite = await applicant.invites.update({mission: updateTeam.mission}, {accepted: true}, {$upsert: true});
    let applicantInvite = applicant.invites.map(invite=>{
        if(invite.mission.toString() === updateTeam.mission.toString()){
            console.log('yes they are equal');
            invite.accepted = true
            return invite;
        }
        return invite;
    });
    applicant.invites = applicantInvite;
    // await applicant.save()

    competencies = await Competency.find({});
    mission = updateTeam.mission;

    console.log(`The applicant has joined the team`);

    res.render('mission/fillCompetencies', {
        pageTitle: 'Qiesto - Competencies to Join Team',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        team: updateTeam,
        competencies: competencies,
        mission
       
    });
}

exports.viewApplicantProfile = async(req, res, next) =>{

    let applicantId = req.params.applicantId;
    // console.log(applicantId);
    let applicantNotification = await Applicant.findOne({_id: res.locals.applicant._id});
    let notifications = applicantNotification.notifications;
    let unreadNotifications = applicantNotification.notifications.filter(notification=> notification.status === false)
    
    return res.render('applicant/applicant-profile', {
        pageTitle: 'Qiesto - Mission - ',
        errorMessage: false,
        successMessage: false,
        validationErrors: [],
        oldInput: [],
        notifications,
        unreadNotifications
    })
}

exports.getNotifications = async(req, res, next) =>{
    let teamId = req.params.teamId;
    let team = await Team.findById(teamId);

    let currentNotifications = res.locals.applicant.notifications;

    const personalDetails = await Applicant.findById(res.locals.applicant._id);
    // const personalDetails = res.locals.applicant;
    const missions = await Mission.find().populate('categories.category').populate('company').limit(10);

    // console.log(personalDetails.work);

    let applicantNotification = await Applicant.findOne({_id: res.locals.applicant._id});
    let notifications = applicantNotification.notifications;
    let unreadNotifications = applicantNotification.notifications.filter(notification=> notification.status === false);

    let modifiedNotifications = markNotificationsAsRead(applicantNotification.notifications);

    console.log('the modified notification ')
    console.log(modifiedNotifications);

    applicantNotification.notifications = modifiedNotifications;

    await applicantNotification.save();
    
    console.log('these are the notifications ');
    console.log(currentNotifications);
    return res.render('applicant/applicant-notifications', {
        pageTitle: 'Qiesto - Team Notification',
        errorMessage: false,
        successMessage: req.session.successMessage,
        validationErrors: [],
        oldInput: [],
        personalDetails: personalDetails,
        missions: missions,
        notifications,
        unreadNotifications,
        currentNotifications
    });
}


const markNotificationsAsRead = (notifications) => {
    let returnedNotification = notifications.map(notification => {
         notification.status = true
         return notification
    });

    return returnedNotification;
}
// exports.viewApplicantProfile = async (req, res, next) => {

exports.getProfile = async (req, res, next) => {

    let applicantId = req.params.applicantId;

    // console.log(applicantId);

    const applicantDetails = await Applicant.findOne({ '_id': applicantId })
                                            .populate('missions');

    // console.log(applicantDetails.firstname)


    return res.render('applicant/profile', {
        pageTitle: 'Qiesto - Applicant public profile',
        errorMessage: false,
        successMessage: req.session.successMessage,
        validationErrors: [],
        oldInput: [],
        applicantDetails: applicantDetails,
        // missions: missions,
        // notifications,
        // unreadNotifications,
        // currentNotifications
    });
}