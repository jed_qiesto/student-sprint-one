const path = require('path');
const { validationResult } = require('express-validator');
// const bcrypt = require('bcryptjs');
const Applicant = require('../models/applicant');
const mongoose = require('mongoose');
const Mission = require('../models/mission');
const Company = require('../models/company');
const Category = require('../models/category');
const Jobrole = require('../models/jobrole');
const Team = require('../models/team');
const Interest = require('../models/interest');
const moment = require('moment');
const faker = require('faker');



exports.seedJobrole = async (req, res, next) => {

    for (c = 0; c <= 10; c++) {
        //create 10 Jobroles
        jobrole = new Jobrole({
            name: faker.name.jobTitle(),
            description: faker.name.jobDescriptor(),
        });

        jobrole = await jobrole.save();

        console.log(jobrole);

    }

    return 'Jobroles have been completed seeded!';
};

exports.seedCategory = async (req, res, next) => {

    for (c = 0; c <= 10; c++) {
        //create 10 Categories
        category = new Category({
            name: faker.random.word(),
            description: faker.lorem.paragraph(),
        });
        category = await category.save();

        console.log(category);

    }

    return 'Categories have been completed seeded!';
};

exports.seedCompany = async (req, res, next) => {

    for (c = 0; c <= 5; c++) {
        //create 10 missions
        company = new Company({
            name: faker.name.title(),
            logo: 'http://134.209.17.237/sample2/img/qiesto_logo_dark.png',
            description: faker.lorem.paragraph(),
        });

        company = await company.save();

        console.log(company);
    }

    return 'Company Seed Completed';
};

exports.seedMission = async (req, res, next) => {

    // randomCategory = await Category.findOneRandom();
    let category;
    


    // console.log(categories);

    for (c=0; c<=10; c++){

        let categories = [];
        let jobroles = [];
        let company;

        //Randomly Select Categories
        let results = await Category.aggregate([{ $sample: { size: 3 } }]);
        for (let result of results) //array is your array variable, i suppose
            categories.push({ _id: mongoose.Types.ObjectId(), category: result._id });

        //Randomly Select Jobroles
        let JobResults = await Jobrole.aggregate([{ $sample: { size: 5 } }]);
        for (let result of JobResults) //array is your array variable, i suppose
            jobroles.push({ _id: mongoose.Types.ObjectId(), jobrole: result._id });
       
        // Randomly Select Company
        let companyResults = await Company.aggregate([{ $sample: { size: 1 } }]);
        for (let result of companyResults) //array is your array variable, i suppose
            company = result._id;

        // console.log(company);

        let randomMonth  = Math.ceil(Math.random() * 12);
        let randomDay  = Math.ceil(Math.random() * 28);

        let solver = ['individual', 'team','individual', 'team','individual', 'team'];
        // create 10 missions
        mission = new Mission({
            title: faker.name.title(),
            objective: faker.lorem.paragraphs(),
            company: company,
            published: true,
            solver: solver[Math.ceil((Math.random() * 5))],
            startDate: new Date(),
            endDate: new Date(2020, randomMonth, randomDay ),
            categories: categories,
            interested:[],
            jobroles: jobroles,
            submissionsCount: 0
        });

        mission.save();

    }

    return 'Mission seed has been added';
    // res.render('applicant/profile', {
    //     pageTitle: 'Qiesto - Applicant',
    //     errorMessage: false,
    //     successMessage: false,
    //     validationErrors: [],
    //     oldInput: [],
    // });
};