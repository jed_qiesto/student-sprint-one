'use strict';
const nodeMailer = require('nodemailer');
const nodemailMailgun = require("nodemailer-mailgun-transport");
const Email = require('email-templates');

exports.sendMail = function(req,res){
    const auth = {
        auth:{
        api_key:'1f649ca2306e5f6114afff1141c63c6e-73ae490d-19504fd4', 
            domain:'email.qiesto.com' 
        }
    }

    let transporter = nodeMailer.createTransport(nodemailMailgun(auth));
    const email = new Email({
      transport: transporter,
      send: true,
      preview: false,
      views: {
        options: {
          extension: 'ejs',
        },
        root: './emails',
      },
    });
    
    email.send({
      template: 'emailtemplates',
      message: {
        from: 'accounts@qiesto.com',
        to: 'recipient@gmail.com',
      },
      locals: {
        fname: 'Jedidiah',
        lname: 'Anthony',
      }
    }).then(() => console.log('email has been sent!'));
  
    
}