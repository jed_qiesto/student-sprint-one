var express = require('express');

    app = express(),
    port = process.env.PORT || 4000;
    const nodemailer = require('nodemailer');
    const Email = require('email-templates');
    //include route file for the mail
    mailRoute = require('./route');

    mailRoute(app);

app.get('/', function (req, res) {

});

app.listen(port, function (err) {
    if (err) {
        console.log(err);
    }
    console.info('>>> 🌎 Open http://localhost:%s/ in your browser.', port);
  })
  
  