'use strict';
module.exports = function(app) {
  
  var SendMailController = require('./sendMail');

  app.route('/sendmail').get(SendMailController.sendMail);

};