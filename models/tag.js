const mongoose = require('mongoose');
// const random = require('mongoose-simple-random');
 
 
const Schema = mongoose.Schema;
 
const tagSchema = new Schema({
    name: {
        type: String,
        required: true
    }
},
{ timestamps: true });
 
// categorySchema.plugin(random);
 
module.exports = mongoose.model('Tag', tagSchema);