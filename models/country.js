const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const countrySchema = new Schema({
    name: {
        common: {
            type: String,
            required: true
        },
        native: {
            eng: {
                common: {
                    type: String,
                    required: true
                },
                official: {
                    type: String,
                    required: true
                }
            }
        }
    }
},
    { timestamps: true });
module.exports = mongoose.model('Country', countrySchema);
