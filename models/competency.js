const mongoose = require('mongoose');

// var deepPopulate = require('mongoose-deep-populate')(mongoose)

const Schema = mongoose.Schema;

const competencySchema = new Schema({
    competency: [
        {
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: false
        },
    }]
    // competencies: {
    //     competency: [
    //         {
    //             name: {
    //                 type: String,
    //                 required: true
    //             },
    //             description: {
    //                 type: String,
    //                 required: false
    //             }
    //         }
    //     ]
    // }
},
    { timestamps: true });

module.exports = mongoose.model('Competency', competencySchema);