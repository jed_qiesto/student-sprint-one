const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const companySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    logo: {
        type: String,
        required: false
    },
    industry: {
        type: Schema.Types.ObjectId,
        ref: 'Industry',
        required: false
    },
    companyDescription: {
        type: String,
        required: false
    },
    address: {
        type: String,
        required: false
    },
    country: {
        type: Schema.Types.ObjectId,
        ref: 'Country',
        required: false
    },
    source: {
        type: Schema.Types.ObjectId,
        ref: 'Source',
        required: false
    },
    missions: [{
        type: Schema.Types.ObjectId,
        ref: 'Mission',
        required: false
    }]
},
    { timestamps: true });

module.exports = mongoose.model('Company', companySchema);