const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const applicantSchema = new Schema({
    profilePicture: {
        type: String,
        required: false
    },
    firstname: {
        type: String,
        required: true
    },
    middlename: {
        type: String,
        required: false
    },
    lastname: {
        type: String,
        required: true
    },
    isVerified: {
        type: Boolean,
        default: false
    }, 
    verificationToken: {
        type: String,
        required: false
    },
    birthday: {
        type: Date,
        required: false
    },
    gender: {
        type: String,
        required: false
    },
    phoneNumber: {
        type: String,
        required: false
    },
    country: {
        type: Schema.Types.ObjectId,
        ref: 'Country',
        required: false
    },
    occupation: {
        type: String,
        required: false
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    resetToken: String,
    resetTokenExpiration: Date,
    source: {
        type: Schema.Types.ObjectId,
        ref: 'Source',
        required: true
    },
    facebook: {
        type: String,
        required: false
    },
    linkedin: {
        type: String,
        required: false
    },
    twitter: {
        type: String,
        required: false
    
    },
    socialId: {
        type: String,
        required: false
    
    },
    displayName: {
        type: String,
        required: false
    
    },
    about: {
        type: String,
        required: false
    },
    interests: [
        {
            interest: {
                type: Schema.Types.ObjectId,
                ref: 'Interest',
                required: false
            }, 
            other: {
                type: String,
                required: false
            }
        }

    ],
    education: [
        {
            school: {
                type: String,
                required: false
            },
            course: {
                type: String,
                required: false
            },
            startYear: {
                type: String,
                required: false
            },
            endYear: {
                type: String,
                required: false
            }
        }
    ],
    work: [
        {
            employer: {
                type: String,
                required: false
            },
            role: {
                type: String,
                required: false
            },
            startYear: {
                type: String,
                required: false
            },
            endYear: {
                type: String,
                required: false
            },
            description: {
                type: String,
                required: false
            }
        }
    ],
    missions: [
        {
            membership: {
                type: String,
                required: true
            },
            mission: {
                type: Schema.Types.ObjectId,
                ref: 'Mission',
                required: true
            },
            team: {
                    type: Schema.Types.ObjectId,
                    ref: 'Team',
                    required: true
            },
            position: {
                type: String,
                required: true
            },
            endorsements: [{
                competency: {
                    type: Schema.Types.ObjectId,
                    ref: 'Competency',
                    required: false
                }
            }],
            competencies: [{
                competency: {
                    type: Schema.Types.ObjectId,
                    ref: 'Competency',
                    required: true
                }
            }]
        }],
        invites: [
            {
                mission: {
                    type: Schema.Types.ObjectId,
                    ref: 'Mission',
                    required: true
                },
                team: {
                    type: Schema.Types.ObjectId,
                    ref: 'Team',
                    required: true
                },
                accepted: {
                    type: Boolean,
                    required: false
                }
            }
        ],
        notifications: [
            {
            brief: {
                type: String,
                required: false
            },
            content: {
                type: String,
                required: false
            },
            status: {
                type: Boolean,
                required: false
            },
            dateAdded: {
                type: Date,
                default: new Date()
            },
             // This is to know if the notification will contain a link or not
             notificationType: {
                type: String,
                required: false
            }
        }
        ]
},
    { timestamps: true });

module.exports = mongoose.model('Applicant', applicantSchema);