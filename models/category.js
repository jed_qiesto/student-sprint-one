const mongoose = require('mongoose');
// const random = require('mongoose-simple-random');


const Schema = mongoose.Schema;

const categorySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    }
},
{ timestamps: true });

// categorySchema.plugin(random);

module.exports = mongoose.model('Category', categorySchema);