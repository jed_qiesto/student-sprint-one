const mongoose = require('mongoose');
 
const Schema = mongoose.Schema;
 
const missionSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    startDate: {
        type: Date,
        required: false
    },
    endDate: {
        type: Date,
        required: false
    },
    company: {
        type: Schema.Types.ObjectId,
        ref: 'Company',
        required: true
    },
    description: {
        type: String,
        required: false
    },
    output: {
        type: String,
        required: false
    },
    published: {
        type: Boolean,
        required: true
    },
    solver: {
        type: String,
        required: true
    },
    reward: {
        type: String,
        required: false
    },
    rewardDistribution: {
        type: Number,
        required: false
    },
    submissionsCount: {
        type: Number,
        required: true
    },
    assessments: [{
        assessment: {
            type: Schema.Types.ObjectId,
            ref: 'Assessment',
            required: false
        }
    }],
    interested: [{
        applicant: {
            type: Schema.Types.ObjectId,
            ref: 'Applicant',
            required: false
        }
    }],
    categories: [{
        category: {
            type: Schema.Types.ObjectId,
            ref: 'Category',
            required: false
        }
    }],
    tags: [{
        tag: {
            type: Schema.Types.ObjectId,
            ref: 'Tag',
            required: false
        }
    }],
    teams: [{
        team: {
            type: Schema.Types.ObjectId,
            ref: 'Team',
            required: true
        }
    }],
    jobroles: [{
        jobrole: {
            type: Schema.Types.ObjectId,
            ref: 'Jobrole',
            required: true
        }
    }],
    announcements: [{
        title: {
            type: String,
            required: true
        },
        body: {
            type: String,
            required: true
        },

        created_at: {
            default: new Date(),
            type: Date
        }
    }]
},

{ timestamps: true });
 
module.exports = mongoose.model('Mission', missionSchema);