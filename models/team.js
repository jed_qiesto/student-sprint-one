const mongoose = require('mongoose');

// var deepPopulate = require('mongoose-deep-populate')(mongoose)

const Schema = mongoose.Schema;

const teamSchema = new Schema({
    mission: {
        type: Schema.Types.ObjectId,
        ref: 'Mission',
        required: true
    },
    missionStatus: {
        // True for completed missions
        type: Boolean,
        required: true
    },
    type: {
        // True for completed missions
        type: String ,
        required: true
    },
    dateSubmitted: {
        type: Date,
        required: false
    },
    name: {
        type: String,
        required: true
    },
    who: {
        type: String,
        required: false
    },
    textManifesto: {
        type: String,
        required: false
    },
    videoManifesto: {
        type: String,
        required: false
    },
    missionReport: {
        type: String,
        required: false
    },
    competencies: [{
        competency: {
            type: Schema.Types.ObjectId,
            ref: 'Competency',
            required: false
        }
    }],
    memberCount: {
        type: Number,
        required: false
    },
    members: [{
        membership: {
            type: String,
            required: true
        },
        position: {
            type: String,
            required: true
        },
        member: {
            type: Schema.Types.ObjectId,
            ref: 'Applicant',
            required: true
        },
        competencies: [{
            competency: {
                type: Schema.Types.ObjectId,
                ref: 'Competency',
                required: false
            }
        }]
    }],
    newMemberRequests: [{
        membership: {
            type: String,
            required: true
        },
        position: {
            type: String,
            required: true
        },
        member: {
            type: Schema.Types.ObjectId,
            ref: 'Applicant',
            required: true
        },
        competencies: [{
            competency: {
                type: Schema.Types.ObjectId,
                ref: 'Applicant',
                required: true
            }
        }]
    }],
    notifications: [
        {
            brief: {
                type: String,
                required: false
            },
            content: {
                type: String,
                required: false
            },
            status: {
                type: Boolean,
                required: false
            },
            dateAdded: {
                type: Date,
                default: new Date()
            },
            // This is to know if the notification will contain a link or not
            notificationType: {
                type: String,
                required: false
            }
        }
    ],
    sentInvites: [
        {
            applicant: {
                type: Schema.Types.ObjectId,
                ref: 'Applicant',
                required: true
            },
            accepted: {
                type: Boolean,
                required: false
            }
        }
    ],
    rateMembers: [
        {
            personRating: {
                type: Schema.Types.ObjectId,
                ref: 'Applicant',
                required: true
            },
            personRated: {
                type: Schema.Types.ObjectId,
                ref: 'Applicant',
                required: true
            },
            rating: [
                {
                    competency: {
                        type: Schema.Types.ObjectId,
                        ref: 'Competency',
                        required: true
                    },
                    stars: {
                        type: String,
                        required: false
                    }
                }
            ]
        }
    ],
    jobroles: [{
        jobrole: {
            type: Schema.Types.ObjectId,
            ref: 'Jobrole',
            required: false
        }
    }],
},
    { timestamps: true });

// teamSchema.methods.addToMember = function (member) {
//     //console.log(member);
//     const membersIndex = this.members.findIndex(cp => {
//         return cp.member.toString() === member.toString();
//     });
//     const oldMembers = [...this.members];

//     //console.log(oldMembers);

//     if (membersIndex === 0) {
//         console.log('here');
//         oldMembers.push({
//             member: member
//         });
//     }
//     // console.log(oldMembers);

//     const updatedMembers = {
//         members: oldMembers
//     };
//     this.members = updatedMembers ;
//     return this.save();
// };

// teamSchema.methods.removeFromMemberRequests = function (member) {
//     const updatedMemberRequests = this.newMemberRequests.filter(member => {
//         return member.toString() !== member.toString();
//     });
//     this.newMemberRequests = updatedMemberRequests;
//     return this.save();
// };

// teamSchema.plugin(deepPopulate);

// Team.deepPopulate(newMemberRequest, 'newMemberRequests.competencies', function (err, _posts) {
//     // _posts is the same instance as posts and provided for convenience
//     newMemberRequest.forEach(function (newMemberRequest) {
//         // post.comments and post.comments.user are fully populated
//         newMemberRequest.competencies.competency
//     });
// });

module.exports = mongoose.model('Team', teamSchema);