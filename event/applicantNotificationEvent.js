exports.triggerNotification = (io, applicant)=>{
    io.on('connection', (socket)=>{
        console.log('this user has been triggered');
        let {notifications} = applicant;
        let unreadNotifications = notifications.filter(notification=> notification.status === false);
        socket.on('login', async(userData) =>{
            console.log('The socket has been triggered');
            console.log(userData);
            console.log(`This is the memberName id ${applicant._id}`);
    
            io.emit(applicant._id, {unreadNotifications, notifications, applicant});
        })
        
    })
}