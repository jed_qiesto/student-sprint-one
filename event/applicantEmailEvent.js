const SendMailController = require('../controllers/email');

exports.sendTeamInvite = (recipientEmail, firstname, teamName) => {
    SendMailController.sendTeamInvite(recipientEmail, firstname, teamName);
};

exports.sendMissionComplete = (recipientEmail, firstname, mission, team, teamId ) =>{
    SendMailController.sendMissionComplete(recipientEmail, firstname, mission, team, teamId );
};

exports.sendLoginMail = (email, firstname, token)=>{
    SendMailController.sendLoginMail(email, firstname, token);
};

exports.sendVerificationMail = (email, firstname, token)=>{
    SendMailController.sendVerificationMail(email, firstname, token);
};

exports.sendResetPassword = (email, firstname, token) =>{
    SendMailController.sendResetPassword(email, firstname, token); 
};

exports.sendRequestToJoinTeam =(leaderEmail, leaderName, firstname) =>{
    SendMailController.sendRequestToJoinTeam(leaderEmail, leaderName, firstname)
};

exports.sendJoinTeamNotification = (recipientEmail, recipientFirstName, teamName, newMember) =>{
    SendMailController.sendJoinTeamNotification(recipientEmail, recipientFirstName, teamName, newMember);  
};