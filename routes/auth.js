const express = require('express');
const { check, body } = require('express-validator');

const authController = require('../controllers/auth');
const Applicant = require('../models/applicant');
const isVerified = require('../middleware/is-verified');
var passportLinkedIn = require('../controllers/auth/linkedin');


const router = express.Router();


router.get('/', authController.getLogin);

router.get('/info', authController.getNotify);

router.get('/signup', authController.getSignup);

router.post(
    '/signup',
    [
        body('firstname')
            .trim()
            .isLength({ min: 3 })
            .withMessage('Please enter a valid input.'),
        body('lastname')
            .trim()
            .isLength({ min: 3 })
            .withMessage('Please enter a valid input.'),
        body('email')
            .isEmail()
            .trim()
            .isLength({ min: 3 })
            .withMessage('Please enter a valid email address.')
            .custom((value, { req }) => {
                return Applicant.findOne({ email: value }).then(applicantDoc => {
                    if (applicantDoc) {
                        return Promise.reject(
                            'E-Mail exists already, please pick a different one.'
                        );
                    }
                });
            }),
        body(
            'password',
            'Please enter a password with only numbers and text and at least 5 characters.')
            .isLength({ min: 5 })
            // .isAlphanumeric()
            .trim(),
        body('confirm_password').custom((value, { req }) => {
            if (value !== req.body.password) {
                throw new Error('Passwords have to match!');
            }
                return true;
            })
            .trim(),
        body('source')
            .isLength({ min: 3 })
            .withMessage('Please enter a valid input.'),
    ],
    authController.postSignup
);

router.get('/login', authController.getLogin);

router.post(
    '/login',
    [
        check('email')
            .isEmail()
            .withMessage('Please enter a valid email'),
        check(
            'password',
            'Please enter a password with only numbers and text and at least 5 characters.'
        )
            .isLength({ min: 5 })
            // .isAlphanumeric()
            .trim()
    ],  
    authController.postLogin
);



router.post('/logout', authController.postLogout);

router.get('/logout', authController.postLogout);

router.get('/reset', authController.getReset);

router.post('/reset', authController.postReset);

router.get('/reset/:token', authController.getNewPassword);

router.post('/new-password', [
    body(
    'password',
    'Please enter a password with only numbers and text and at least 5 characters.')
.isLength({min: 5})
.isAlphanumeric()
.trim(),
body('confirm_password').custom((value, { req }) => {
    if (value !== req.body.password) {
        throw new Error('Passwords do not match!');
    }
        return true;
    })
    .trim(),] , 
    authController.postNewPassword);


    router.get('/authVerify/:token', authController.getEmailVerification);

    router.get('/terms', authController.getTermsAndConditions);

router.get('/auth/linkedin', passportLinkedIn.authenticate('linkedin'));

router.get('/auth/linkedin/callback',
    passportLinkedIn.authenticate('linkedin', { failureRedirect: '/login' }),
    function (req, res) {
        // Successful authentication
        res.json(req.user);
        
        let applicant = req.user;
        req.session.isLoggedIn = true;
        req.session.isVerified = applicant.isVerified;
        req.session.applicant = applicant;
        req.session.notifications = applicant.notifications;
        req.session.teamNotifications = teamNotifications;
        req.session.applicantTeamId = applicantTeamId;

        if (!applicant.interests) {
            return res.redirect('/interest');
            // return res.redirect('/setup/interest');
        }
        return req.session.save(err => {
            res.redirect('/dashboard');
        });
    });
module.exports = router;
