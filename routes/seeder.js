const express = require('express');

const seederController = require('../controllers/seeder');

const router = express.Router();

router.get('/seed/company', seederController.seedCompany);
router.get('/seed/category', seederController.seedCategory);
router.get('/seed/mission', seederController.seedMission);
router.get('/seed/jobrole', seederController.seedJobrole);


module.exports = router;
