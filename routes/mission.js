const express = require('express');
const { check, body } = require('express-validator');

const missionController = require('../controllers/mission');
const teamController = require('../controllers/team');
const Applicant = require('../models/applicant');
const isAuth = require('../middleware/is-auth');

const router = express.Router();



router.get('/missions', isAuth, missionController.getMissions);

router.get('/missions/:missionId/teams', isAuth, missionController.getTeams);
router.get('/mymissions?:selected', isAuth, missionController.getMyMissions);

router.get('/missions/mission/:missionId', isAuth, missionController.getMission);

// router.get('/:missionName/team/create', isAuth, teamController.getCreateTeam);


router.get('/missions/dashboard/:missionId', isAuth, missionController.getMyMission);

router.get('/missions/interested/:missionName', isAuth, missionController.getInterested);

router.get('/missions/:missionId/:teamId/complete', isAuth, missionController.getCompleteMission);

router.get('/missions/:missionId/:teamId/edit', isAuth, missionController.editCompleteMission);

router.post('/missions/:missionId/:teamId/edit', isAuth, missionController.updateCompleteMission);
router.post('/missions/team/:teamId/complete', isAuth, 
            [
                body('report')
                    .isLength({ min: 200 })
                    .withMessage('Please enter a detailed report'),
            ], missionController.postCompleteMission);

router.get('/missions/completed/:teamId', isAuth, missionController.getCompletedMission);

router.get('/missions/filter/:name/type/:type', isAuth, missionController.getByFilter);


router.get('/missions/filter/:name/type/:type', isAuth, missionController.getByFilter);

router.get('/missions/advsearch', isAuth, missionController.getAdvanceSearch);

router.post('/missions/advsearch', isAuth, missionController.postAdvanceSearch);


router.get('/missions/filter/:name/type/:type', isAuth, missionController.getByFilter);

router.get('/missions/advsearch', isAuth, missionController.getAdvanceSearch);

router.post('/missions/advsearch', isAuth, missionController.postAdvanceSearch);

// router.get('/missions/team/:teamId/notifications', isAuth, teamController.getNotifications);

// router.get('/missions/team/:teamId/accept', isAuth, teamController.storeNewApplicant);
// router.post('/logout', authController.postLogout);

// router.get('/reset', authController.getReset);

// router.post('/reset', authController.postReset);

// router.get('/reset/:token', authController.getNewPassword);

// router.post('/new-password', authController.postNewPassword);

module.exports = router;
