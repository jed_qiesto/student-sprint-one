const express = require('express');
const { check, body } = require('express-validator');

const missionController = require('../controllers/mission');
const teamController = require('../controllers/team');
const Applicant = require('../models/applicant');
const isAuth = require('../middleware/is-auth');

const router = express.Router();



router.post('/team/create',
    isAuth,
    [
        body('teamName')
            .isString()
            .isLength({ min: 3 })
            .trim()
            .withMessage('Please enter a valid name'),
    ],
    teamController.postCreateTeam);

router.get('/viewTeams', isAuth, teamController.getTeams)
router.get('/:missionName/team/create', isAuth, teamController.getCreateTeam);


router.get('/team/:teamId/join', isAuth, teamController.getJoinTeam);
router.post('/team/join', isAuth, teamController.postJoinTeam);

router.get('/team/:teamId/invite', isAuth, teamController.inviteMembers);

router.post('/team/:teamId/invite', isAuth, teamController.addTeamMembers);

router.get('/teams', isAuth, teamController.getMyTeams);
router.get('/teams/:teamId/:memberId/approve', isAuth, teamController.approveMember);


router.get('/missions/team/:teamId/notifications', isAuth, teamController.getNotifications);

router.get('/missions/team/:teamId/accept', isAuth, teamController.getNewApplicantRevised);

router.post('/missions/team/:teamId/accept', isAuth, teamController.storeNewApplicant)

// ANDY'S METHOD OF ADDING TEAM MEMBERS
router.get('/team/:teamId/:applicantId/invite-new', isAuth, teamController.addTeamMembersAndy);

router.get('/team/rateMembers/:teamId', isAuth, teamController.getRateMembers);

router.post('/team/rateMembers/:teamId', isAuth, teamController.postRateMembers);

router.get('/team/viewRating/:teamId', isAuth, teamController.getViewMemberRating);

router.get('/missions/individual/:missionId', isAuth, teamController.getCreateIndividualTeam);
router.post('/missions/individual/create', isAuth, 
// [
//     check('myCompetencies')
//         .exists()
//         .withMessage('Please select a valid competencies'),
// ],
teamController.postCreateIndividualTeam);
// router.post('/logout', authController.postLogout);

// router.get('/reset', authController.getReset);

// router.post('/reset', authController.postReset);

// router.get('/reset/:token', authController.getNewPassword);

// router.post('/new-password', authController.postNewPassword);

module.exports = router;
