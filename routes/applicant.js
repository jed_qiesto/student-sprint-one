const express = require('express');
const { check, body } = require('express-validator');

// const missionController = require('../controllers/mission');
const applicantController = require('../controllers/applicant');
const Applicant = require('../models/applicant');
const isAuth = require('../middleware/is-auth');

const router = express.Router();


router.get('/settings', isAuth, applicantController.getSettings);
router.post('/settings', 
        isAuth, 
         [
            body('firstname')
                .isString()
                .isLength({ min: 3 })
                .trim()
                .withMessage('Please enter a valid firstname'),
            body('lastname')
                .isString()
                .trim()
                .isLength({ min: 3 })
                .withMessage('Please enter a valid lastname'),
        ],
        applicantController.postSettings);
router.get('/settings/education', isAuth, applicantController.getEducation);
router.post('/settings/education', isAuth, applicantController.postEducation);

router.get('/settings/work', isAuth, applicantController.getWork);
router.post('/settings/work', isAuth, applicantController.postWork);

router.get('/profile/:applicantId', 
        isAuth, 
        applicantController.getProfile);


router.get('/setup/info', isAuth, applicantController.getSetupInfo);
router.post('/setup/info',
        isAuth,
        [
                body('firstname')
                        .isString()
                        .isLength({ min: 3 })
                        .trim()
                        .withMessage('Please enter a valid firstname'),
                body('lastname')
                        .isString()
                        .trim()
                        .isLength({ min: 3 })
                        .withMessage('Please enter a valid lastname'),
                body('country')
                        .isString()
                        .trim()
                        .isLength({ min: 3 })
                        .withMessage('This field cannot be empty'),
                body('phoneNumber')
                        .isString()
                        .trim()
                        .isLength({ min: 3 })
                        .withMessage('Please enter a valid phone number'),
                body('about')
                        .isString()
                        .trim()
                        .isLength({ min: 15 })
                        .withMessage('Please tell us about yourself'),
                body('occupation')
                        .isString()
                        .trim()
                        .isLength({ min: 3 })
                        .withMessage('Please enter a valid Occupation/Expertise'),
        ],
        applicantController.postSettings);
router.get('/setup/interest', isAuth, applicantController.getSetupInterest);
router.post('/setup/interest', isAuth, applicantController.postInterest);

router.get('/dashboard', isAuth, applicantController.dashboard);

router.get('/myinvites', isAuth, applicantController.getInvites);
router.get('/:teamId/acceptInvite', isAuth, applicantController.acceptInvite);


router.get('/profiles/:applicantId', isAuth, applicantController.viewApplicantProfile);
// router.get('/profile/:applicantId', isAuth, applicantController.getProfile);
router.get('/applicant/notifications/:applicantId', isAuth, applicantController.getNotifications);
module.exports = router;
