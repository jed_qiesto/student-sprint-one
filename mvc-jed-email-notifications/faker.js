import jsf from 'json-schema-faker';

const schema = {
    type: 'object',
    properties: {
        user: {
            type: 'object',
            properties: {
                id: {
                    $ref: '#/definitions/positiveInt'
                },
                name: {
                    type: 'string',
                    faker: 'name.findName'
                },
                email: {
                    type: 'string',
                    format: 'email',
                    faker: 'internet.email'
                }
            },
            required: ['id', 'name', 'email']
        }
    },
    required: ['user'],
    definitions: {
        positiveInt: {
            type: 'integer',
            minimum: 0,
            exclusiveMinimum: true
        }
    }
};

// use the async-version (preferred way)
jsf.resolve(schema).then(sample => {
    console.log(sample);
    // "[object Object]"

    console.log(sample.user.name);
    // "John Doe"
});

// sync-version (blocking)
jsf.generate(schema); // [object Object]