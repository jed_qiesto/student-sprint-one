const Applicant = require('../models/applicant');

exports.getAllNotifications = async (authId) =>{
    notifications = [];
    unreadNotifications = [];
    let applicant = await Applicant.findOne({_id: authId});
    if(applicant){
        notifications = applicant.notifications;
        unreadNotifications = notifications.filter(notification=> notification.status === false);
       
   
}
return {notifications, unreadNotifications, applicant};
}
