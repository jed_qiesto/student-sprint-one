const Categories = require('../models/category');
const Companies = require('../models/company');
const Missions = require('../models/mission');


exports.getAllSearchOptions = async (authId) =>{
    let CATEGORY = 'CATEGORY';
    let COMPANY = 'COMPANY';
    let MISSION = 'MISSION';

    notifications = [];
    unreadNotifications = [];
    
    let categories = await Categories.find({});
    let companies = await Companies.find({});
    let missions = await Missions.find({});

    let parsedModels = parseData(categories, CATEGORY);
   parsedModels =  parsedModels.concat(parseData(companies, COMPANY));
   parsedModels =  parsedModels.concat(parseData(missions, MISSION));

return  parsedModels;
}

const parseData = (data, model) => {
    let parsedArray = [];
    switch (model) {
        case 'CATEGORY' :
            data.forEach(dat=> {
                parsedArray.push(
                    {name: dat.name, 
                    type: 'category'});
            });
            break;
        case 'COMPANY' : 
            data.forEach(dat=> {
                parsedArray.push(
                    {name: dat.name, 
                    type: 'company'});
            });
            break;

        case 'MISSION' :
            data.forEach(dat=> {
                parsedArray.push( 
                    {name : dat.title,
                    type: 'mission'}
                );
            })
            break;
        default: 
        break;
    }

    return parsedArray;
}