const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const mongoose = require('mongoose');
const csrf = require('csurf');
const flash = require('connect-flash');
// var fs = require('fs');

const errorController = require('./controllers/error');

const {getAllNotifications} = require('./helpers/notificationHelper');

const sharedSession = require('express-socket.io-session');
const multer = require('multer');

// const MONGODB_URI = 'mongodb://testAdmin:test@localhost:27017/qiestomvc?authSource=admin';
// const MONGODB_URI = 'mongodb://localhost/qiestomvc';
// const MONGODB_URI = 'mongodb://:@localhost:27017/qiestomvc';

const DB_USER = 'qiestomvc';
const PASSWORD = encodeURIComponent('Notica007');
const MONGODB_URI = `mongodb+srv://${DB_USER}:${PASSWORD}@cluster0-hvmvr.mongodb.net/demomvc?authSource=admin`;


const app = express();

const http = require('http').createServer(app);
const io = require('socket.io')(http);

const store = new MongoDBStore({
    uri: MONGODB_URI,
    collection: 'sessions'
});
const csrfProtection = csrf();

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/assets/media/users');
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString() + '-' + '.' + file.mimetype.split('/')[1])
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg'){
        cb(null, true);
    } else {
        cb(null, false);
    }
    
};

// array used for navbar search
const {getAllSearchOptions} = require('./helpers/searchHelper');
const getAllSearch = async() => {
    let results = await getAllSearchOptions();
    // console.log('the search options ');
    // console.log(results);

// console.log(results);
app.locals.searchOptions = results;
};

getAllSearch();


app.set('view engine', 'ejs');
app.set('views', 'views');

const authRoutes = require('./routes/auth');
const missionRoutes = require('./routes/mission');
const teamRoutes = require('./routes/team');
const applicantRoutes = require('./routes/applicant');
const seederRoutes = require('./routes/seeder');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multer({ storage: fileStorage, fileFilter: fileFilter }).single('profile_avatar'));


// app.use('/static', express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, 'public')));
// app.use(express.static(__dirname + '/public'));
app.use('/public/assets/media/users', express.static(path.join(__dirname, 'images')));
// app.use('/public/css', express.static(path.join(__dirname, 'css')));
// app.use('/public/css', express.static(path.join(__dirname, 'css')))

app.use(
    session({
        secret: 'my secret',
        resave: false,
        saveUninitialized: false,
        store: store
    })
);

app.use(csrfProtection);
app.use(flash());

app.use((req, res, next) => {

    res.locals.successMessage = req.session.successMessage;
    res.locals.errorMessage = req.session.errorMessage;
    
    res.locals.isAuthenticated = req.session.isLoggedIn ;
    res.locals.isVerified = req.session.isVerified;
    res.locals.applicant = req.session.applicant;
    app.locals.moment = require('moment');
    res.locals.csrfToken = req.csrfToken();
    // res.locals.notifications = req.session.notifications;
    // res.locals.teamNotifications = req.session.teamNotifications;
    res.locals.applicantTeamId = req.session.applicantTeamId;
    next();
});
//app.use(bodyParser.json()); //application json

app.use('/', authRoutes);
app.use('/', missionRoutes);
app.use('/', teamRoutes);
app.use('/', applicantRoutes);
app.use('/', seederRoutes);
app.use(errorController.get404);



// FOR SOCKET io
io.use(sharedSession(
    session({
        secret: 'my secret',
        resave: true,
        saveUninitialized: true,
        store: store
    })
));

app.locals.io = io;
app.locals.notifications = [];
 io.on('connection', (socket)=>{
        // SELECT ALL THE NOTIFICATIONS AND GIVE OUT A NOTIFICATION,
        // THIS SHOULD BE AT SIGN IN
        console.log('a user is connected');
        app.locals.socket = socket;
        socket.on('login', async(userData)=>{
            console.log('I am displaying user data now');
            // console.log(userData)
            socket.handshake.session.userData = userData;
            socket.handshake.session.save();
       
            let {notifications, unreadNotifications, applicant} = await getAllNotifications(userData);
            
            console.log(`Notification Helper ${notifications}`);
            io.emit(userData, {notifications, unreadNotifications, applicant});
            // console.log('I just emitted the user data');
        })
        socket.on('disconnect', ()=>{
            // console.log('user is disconnected');
        });
           
    });

mongoose
    .connect(MONGODB_URI, { useUnifiedTopology: true, useNewUrlParser: true })
    .then(result => {
        console.log('Connected to Database!')
        http.listen(5000,  function(){
            console.log('listening on *:5000');
          });
    })
    .catch(err => {
        console.log(err);
    });

